﻿namespace ArvandCarApp.DependencyInterfaces
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
