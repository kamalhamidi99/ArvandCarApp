﻿using SQLite;

namespace ArvandCarApp.Services.Login
{
    public class LoginModel
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
