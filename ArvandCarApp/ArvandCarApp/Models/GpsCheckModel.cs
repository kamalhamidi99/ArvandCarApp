﻿namespace ArvandCarApp.Models
{
    public class GpsCheckModel
    {
        public bool IsActiveGPS { get; set; }
        public string Message { get; set; }
    }
}
