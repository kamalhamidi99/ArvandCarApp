﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ArvandCarApp.Helpers;
using Newtonsoft.Json;

namespace ArvandCarApp.Services.History
{
    public class HistoryService
    {

        public async Task<IList<HistoryModel>> LoadAsync()
        {
            var httpCarNames = await HttpClientHelper.SendAndReadAsync(HttpMethod.Post,
                $"{ApiUrlHelper.Url}/api/history", isAuthorization: true);
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                var data = JsonConvert.DeserializeObject<IList<HistoryModel>>(httpCarNames.ResponseMessage);
                if (data.Any())
                {
                    return data;
                }
            }

            return new List<HistoryModel>();
        }

    }
}
