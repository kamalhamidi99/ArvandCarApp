﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace ArvandCarApp.Views.Account.MobileNumber
{
    public class MobileNumberViewModel : INotifyPropertyChanged
    {
        #region Fields

        DateTime _endOfTime;

        #endregion

        #region Properties

        private string _timer;
        public string Timer
        {
            get => _timer;
            set
            {
                _timer = value;
                OnPropertyChanged("Timer");
            }
        }

        private bool _timerVisibility;
        public bool TimerVisibility
        {
            get => _timerVisibility;
            set
            {
                _timerVisibility = value;
                OnPropertyChanged("TimerVisibility");
            }
        }

        private string _smsValidateCode;
        public string SmsValidateCode
        {
            get => _smsValidateCode;
            set
            {
                _smsValidateCode = value;
                OnPropertyChanged("SmsValidateCode");
            }
        }

        #endregion

        #region Constructor

        public MobileNumberViewModel()
        {
            _timerVisibility = true;
        }

        #endregion

        public async void RunTimer()
        {
            _endOfTime = DateTime.Now.AddMinutes(5);
            while (true)
            {
                await Task.Delay(1000);

                if (!string.IsNullOrWhiteSpace(App.SmsMobileNumberValidate))
                {
                    _smsValidateCode = App.SmsMobileNumberValidate;
                    OnPropertyChanged("SmsValidateCode");

                    _timerVisibility = false;
                    OnPropertyChanged("TimerVisibility");

                    return;
                }

                var ts = _endOfTime.Subtract(DateTime.Now);
                var splite = ts.ToString().Split('.')[0].Split(':');

                _timer = $"{splite[1]}:{splite[2]}";
                if (_timer.Equals("00:00"))
                {
                    _timerVisibility = false;
                    OnPropertyChanged("TimerVisibility");

                    return;
                }

                OnPropertyChanged("Timer");
            }
        }

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #endregion

    }
}
