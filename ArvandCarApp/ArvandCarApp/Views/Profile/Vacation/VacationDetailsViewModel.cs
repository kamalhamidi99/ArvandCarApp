﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using ArvandCarApp.Models;
using ArvandCarApp.Services.Vacation.VacationItem;
using ArvandCarApp.Services.Vacation.VacationPaymonyItem;

namespace ArvandCarApp.Views.Profile.Vacation
{
    public class VacationDetailsViewModel : INotifyPropertyChanged
    {
        #region Fields

        private bool _isBusy;
        public long VacationId { get; set; }
        private VacationItemModel _model;

        #endregion

        #region Properties

        public bool NoPayment { get; set; }
        public bool DataInfoPayment { get; set; }

        private bool _loadingWait;
        public bool LoadingWait
        {
            get => _loadingWait;
            set
            {
                _loadingWait = value;
                OnPropertyChanged("LoadingWait");
            }
        }

        private int _payButtonGridHeight;
        public int PayButtonGridHeight
        {
            get => _payButtonGridHeight;
            set
            {
                _payButtonGridHeight = value;
                OnPropertyChanged("PayButtonGridHeight");
            }
        }

        private ObservableCollection<KeyValueDto> _vacationDetails;
        public ObservableCollection<KeyValueDto> VacationDetails
        {
            get => _vacationDetails;
            set
            {
                _vacationDetails = value;
                OnPropertyChanged("VacationDetails");
            }
        }

        private ObservableCollection<VacationPaymonyItemModel> _vacationPayment;
        public ObservableCollection<VacationPaymonyItemModel> VacationPayment
        {
            get => _vacationPayment;
            set
            {
                _vacationPayment = value;
                OnPropertyChanged("VacationPayment");
            }
        }


        private bool _showContent;
        public bool ShowContent
        {
            get => _showContent;
            set
            {
                _showContent = value;
                OnPropertyChanged("ShowContent");
            }
        }

        #endregion

        #region Constructor

        public VacationDetailsViewModel()
        {
            
        }

        public VacationDetailsViewModel(long vacationId)
        {
            VacationId = vacationId;

            _vacationDetails = new ObservableCollection<KeyValueDto>();
            _vacationPayment = new ObservableCollection<VacationPaymonyItemModel>();
            _payButtonGridHeight = 0;

            LoadMoreData(300);
        }

        public async Task LoadMoreData(int delay = 0)
        {
            _isBusy = false;
            _loadingWait = true;

            if (delay > 0)
            {
                await Task.Delay(delay);
            }

            //To ignore if items are being added till delayed time.
            if (_isBusy)
                return;

            if (_model == null)
            {
                _model = (await App.VacationItemService.LoadAsync()).FirstOrDefault(x => x.Id == VacationId);
                if (_model == null)
                    return;
            }

            if (_model.IsOk == "1")
            {
                // اجازه پرداخت از توی سرور برای پرداختهای تکی
                if (_model.CanPay.ToLower() != "false")
                {
                    // اجازه پرداخت از توی سرور برای پرداختهای تکی
                    if (!_model.PaymonyList.Any(x => x.SetPayTypeActionSetPay == "3456" && x.IsOk.ToLower() == "true"))
                    {
                        _payButtonGridHeight = 50;
                    }
                }
            }

            _vacationDetails = new ObservableCollection<KeyValueDto>()
            {
                new KeyValueDto
                {
                    Key = "وضعیت",
                    Value = _model.IsOkTitle
                },
                new KeyValueDto
                {
                    Key = "مبلغ",
                    Value = _model.PaymonyList.Any(x => x.SetPayTypeActionSetPay == "3456")
                        ? _model.PaymonyList.First(x => x.SetPayTypeActionSetPay == "3456").SetPaymony
                        : _model.PaymonyList.Sum(x => x.UserPay).ToString(),
                },
                new KeyValueDto
                {
                    Key = "تاریخ درخواست",
                    Value = _model.MorakhasidateRegisterStr
                },
                new KeyValueDto
                {
                    Key = "تاریخ شروع",
                    Value = _model.MorakhasiDateStartStr
                },
                new KeyValueDto
                {
                    Key = "تاریخ پایان",
                    Value = _model.MorakhasiDateEndStr
                },
                new KeyValueDto
                {
                    Key = "پرداخت",
                    Value = _model.IsPayTitle
                },
                new KeyValueDto
                {
                    Key = "محل پلاک",
                    Value = _model.MorakhasiPelakType == "1" ? "اهواز" : "خرمشهر"
                },
            };

            if (_model.PaymonyList.Any(x => x.IsOk.ToLower() == "true"))
            {
                foreach (var item in _model.PaymonyList)
                {
                    item.UserPay = item.UserPay;
                    item.SaleReferenceId = "شماره پیگیری: " + item.SaleReferenceId;
                    item.SetPayDateStr = "تاریخ: " + item.SetPayDateStr + " " + item.SetpayTime;
                }

                foreach (var item in _model.PaymonyList.Where(x => x.IsOk.ToLower() == "true").ToList())
                {
                    if (item.SetPayTypeActionSetPay == "3456")
                        continue;
                    
                    _vacationPayment.Add(item);
                }

                DataInfoPayment = true;
            }
            else
            {
                NoPayment = true;
            }

            OnPropertyChanged("PayButtonGridHeight");
            OnPropertyChanged("VacationPayment");
            OnPropertyChanged("VacationDetails");

            _loadingWait = false;
            OnPropertyChanged("LoadingWait");

            _showContent = true;
            OnPropertyChanged("ShowContent");

            _isBusy = true;
        }

        #endregion

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #endregion

    }
}
