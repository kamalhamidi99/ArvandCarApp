﻿namespace ArvandCarApp.Models
{
    public class RecentClearanceModel
    {
        public string Fullname { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
    }
}
