﻿using System;
using SQLite;

namespace ArvandCarApp.Services.License
{
    public class LicenseModel
    {
        [PrimaryKey]
        public long Id { get; set; }

        public DateTime? StartDate { get; set; }
        public string StartDateStr { get; set; }
        public DateTime? EndDate { get; set; }
        public string EndDateStr { get; set; }
        public int ForYear { get; set; }
        public bool IsFirstYear { get; set; }
        public bool IsPaied { get; set; }
        public string AmountPaied { get; set; }
        public string PercentPaied { get; set; }
        public bool IsAmountModified { get; set; }
        public string Description { get; set; }

        [Ignore]
        public string Color { get; set; }
        [Ignore]
        public bool ShowPaying { get; set; }
        [Ignore]
        public bool ShowPaied { get; set; }
    }
}
