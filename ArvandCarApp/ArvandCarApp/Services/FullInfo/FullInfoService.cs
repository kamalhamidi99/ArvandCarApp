﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ArvandCarApp.Helpers;
using Newtonsoft.Json;
using SQLite;

namespace ArvandCarApp.Services.FullInfo
{
    public class FullInfoService
    {
        readonly SQLiteAsyncConnection _database;

        public FullInfoService()
        {
            _database = App.SqLiteAsyncConnection;
        }

        public async Task<FullInfoModel> GetAsync()
        {
            if (await TableExistAsync())
            {
                var model = await _database.Table<FullInfoModel>().FirstOrDefaultAsync();
                if (model != null)
                    return model;
            }

            return new FullInfoModel();
        }

        private async Task StoreDataAsync(FullInfoModel model)
        {
            try
            {
                await _database.CreateTableAsync<FullInfoModel>();

                model.Id = 1;
                await _database.InsertAsync(model);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }
        }

        public async Task DeleteAsync()
        {
            if (await TableExistAsync())
                await _database.DropTableAsync<FullInfoModel>();
        }

        public async Task<FullInfoModel> LoadAsync()
        {
            var httpCarNames = await HttpClientHelper.SendAndReadAsync(HttpMethod.Post,
                $"{ApiUrlHelper.Url}/api/fullinfo", isAuthorization: true);
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                var data = JsonConvert.DeserializeObject<FullInfoModel>(httpCarNames.ResponseMessage);
                if (data != null)
                {
                    await DeleteAsync();
                    await StoreDataAsync(data);
                }
            }

            return await GetAsync();
        }


        private async Task<bool> TableExistAsync()
        {
            var tableExistsQuery = $"SELECT name FROM sqlite_master WHERE type='table' AND name='{nameof(FullInfoModel)}';";
            var result = await _database.ExecuteScalarAsync<string>(tableExistsQuery);
            if (result != null)
                return true;

            return false;
        }

    }
}
