﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Helpers;
using Newtonsoft.Json;
using SQLite;
using Xamarin.Forms;

namespace ArvandCarApp.Services.NumberValidation
{
    public class NumberValidationService
    {
        readonly SQLiteAsyncConnection _database;

        public NumberValidationService()
        {
            _database = App.SqLiteAsyncConnection;
        }

        public async Task<bool> StoreAsync(NumberValidationModel model)
        {
            try
            {
                if (await TableExistAsync())
                    await DeleteAsync();

                await _database.CreateTableAsync<NumberValidationModel>();

                model.Id = 1;
                var resultAdd = await _database.InsertAsync(model);

                return resultAdd > 0;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                return false;
            }
        }

        public async Task DeleteAsync()
        {
            if (await TableExistAsync())
                await _database.DropTableAsync<NumberValidationModel>();
        }

        public async Task<NumberValidationModel> GetAsync()
        {
            if (await TableExistAsync())
                return await _database.Table<NumberValidationModel>().Where(x => x.Id == 1).FirstOrDefaultAsync();

            return new NumberValidationModel();
        }

        public async Task<bool> ValidateAsync(string validateCode)
        {
            var httpCarNames = await HttpClientHelper.SendAndReadAsync(HttpMethod.Post,
                $"{ApiUrlHelper.Url}/api/CarAppMobileValidate/Validate", isAuthorization: true,
                headers: new Dictionary<string, string>()
                {
                    {"ValidateCode", validateCode}
                });
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                var model = await GetAsync();
                model.IsValidate = true;
                var update = await _database.UpdateAsync(model);
                return update > 0;
            }

            return false;
        }

        public async Task<bool> SendAsync(string mobileNumber)
        {
            var httpCarNames = await HttpClientHelper.SendAndReadAsync(HttpMethod.Post,
                $"{ApiUrlHelper.Url}/api/CarAppMobileValidate/Send", isAuthorization: true,
                headers: new Dictionary<string, string>()
                {
                    {"MobileNumber", mobileNumber}
                });
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                var data = JsonConvert.DeserializeObject<string>(httpCarNames.ResponseMessage);
                if (string.IsNullOrWhiteSpace(data))
                    return false;

                return await StoreAsync(new NumberValidationModel()
                {
                    CreatedOn = DateTime.Now,
                    IsValidate = false,
                    MobileNumber = mobileNumber,
                    ValidationCode = data,
                });
            }

            return false;
        }


        public bool IsValidate()
        {
            var db = DependencyService.Get<ISqLiteService>().DbConnection();

            var resultGet = db.ExecuteScalar<string>($"SELECT name FROM sqlite_master WHERE type='table' AND name='{nameof(NumberValidationModel)}';");
            if (string.IsNullOrWhiteSpace(resultGet))
                return false;

            var model = db.Table<NumberValidationModel>().FirstOrDefault();
            if (model == null || model.Id == 0)
                return false;

            return model.IsValidate;
        }

        private async Task<bool> TableExistAsync()
        {
            var tableExistsQuery = $"SELECT name FROM sqlite_master WHERE type='table' AND name='{nameof(NumberValidationModel)}';";
            var result = await _database.ExecuteScalarAsync<string>(tableExistsQuery);
            if (result != null)
                return true;

            return false;
        }

    }
}
