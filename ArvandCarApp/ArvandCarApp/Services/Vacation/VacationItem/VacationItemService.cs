﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ArvandCarApp.Helpers;
using Newtonsoft.Json;

namespace ArvandCarApp.Services.Vacation.VacationItem
{
    public class VacationItemService
    {

        public async Task<IList<VacationItemModel>> LoadAsync()
        {
            var httpCarNames =
                await HttpClientHelper.SendAndReadAsync(HttpMethod.Post, $"{ApiUrlHelper.Url}/api/vacation", isAuthorization: true);
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrWhiteSpace(httpCarNames.ResponseMessage))
                {
                    var data = JsonConvert.DeserializeObject<IList<VacationItemModel>>(httpCarNames.ResponseMessage);
                    if (data != null)
                    {
                        foreach (var item in data)
                        {
                            item.PaymonyList = await App.VacationPaymonyItemService.GetAllAsync(item.Id.ToString());
                            item.IsOkInt = int.Parse(item.IsOk);
                        }

                        return data.OrderBy(x => x.IsOkInt).ToList();
                    }
                }
            }

            return new List<VacationItemModel>();
        }

    }
}
