﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ArvandCarApp.Helpers;
using ArvandCarApp.Services.Vacation.VacationItem;
using Syncfusion.ListView.XForms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.Vacation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VacationMainView
    {
        private readonly VacationMainViewModel _viewModel;

        public VacationMainView()
        {
            InitializeComponent();

            _viewModel = new VacationMainViewModel();

            BindingContext = _viewModel;

            #region BackPageButton

            var back = new TapGestureRecognizer();
            back.Tapped += BackPageButton_Clicked;
            BackPageButton.GestureRecognizers.Add(back);

            #endregion

        }

        #region VacationItemListView_OnSelectionChanged

        private async void VacationItemListView_OnSelectionChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            var selected = VacationItemListView.SelectedItem;
            if (selected != null)
            {
                VacationItemListView.SelectedItem = null;

                var model = (VacationItemModel)selected;
                var id = model.Id;

                await Navigation.PushAsync(new VacationDetailsView(vacationId: id));
            }
        }

        #endregion

        #region BackPageButton Method

        private async void BackPageButton_Clicked(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        #endregion

        #region LoadData

        private async Task LoadData()
        {
            Background.IsVisible = true;
            BackgroundLoading.IsVisible = true;

            VacationItemListView.ItemsSource = FixData(await App.VacationItemService.LoadAsync());

            Background.IsVisible = false;
            BackgroundLoading.IsVisible = false;
        }

        #endregion

        #region FixData

        private static IList<VacationItemModel> FixData(IList<VacationItemModel> data)
        {
            foreach (var item in data)
            {
                if (item.IsOk == "1" && item.IsPay == "true") // سبز
                {
                    item.Color = "DarkCyan";
                }
                else if (item.IsOk == "2" || item.IsPay == "false") // قرمز
                {
                    item.Color = "DarkRed";
                }
                else
                {
                    item.Color = "CornflowerBlue"; // در حال بررسی
                }

                item.ShowPaying = item.IsPay != "true";
                item.ShowPaied = item.IsPay == "true";
            }

            return data;
        }

        #endregion

        #region Button_Send

        private async void Button_Send(object sender, EventArgs e)
        {
            Background.IsVisible = true;
            BackgroundLoading.IsVisible = true;

            var headers = new Dictionary<string, string>()
            {
                {"MorakhasiType", _viewModel.ButtonMorakhasiType.ToString()},
                {"MorakhasiDayType", _viewModel.ButtonMorakhasiDayType.ToString()},
                {"MonthType", _viewModel.ButtonMonthType.ToString()},
                {"MorakhasiPelakType", _viewModel.ButtonMorakhasiPelakType.ToString()},
            };

            var url = $"{ApiUrlHelper.Url}/api/vacation/add";

            var sendData = await HttpClientHelper.SendAndReadAsync(HttpMethod.Post, url, headers, isAuthorization: true);
            if (sendData.StatusCode == HttpStatusCode.OK)
            {
                await DisplayAlert("درخواست مرخصی", "مرخصی شما با موفقیت ثبت شد.", "بستن");
                await LoadData();
                return;
            }

            Background.IsVisible = false;
            BackgroundLoading.IsVisible = false;

            await DisplayAlert("خطا", sendData.ResponseMessage.Trim('"'), "بستن");
        }

        #endregion

        #region ButtonMorakhasiPelakTypeLabel0

        private void ButtonMorakhasiPelakTypeLabel0_OnClicked(object sender, EventArgs e)
        {
            DisplayAlert("پیام", "مالک محترم در صورت انتخاب مرکز اهواز جهت تحویل پلاک، هزینه ای به عنوان کارمز (265 هزارتومان اعلام شده توسط مرکز اهواز) علاوه بر هزینه های مرخصی از شما در آن مرکز دریافت می گردد، که در صورت انتخاب مرکز خرمشهر این هزینه صفر ریال است.", "موافقم");
        }

        #endregion

    }
}