﻿using Xamarin.Forms;

namespace ArvandCarApp.Custom
{
    public class NoBorderEditor : Editor
    {
        public static readonly BindableProperty PlaceholderProperty =
            BindableProperty.Create(nameof(NoBorderEditor), typeof(string), typeof(string), string.Empty);
        public string Placeholder
        {
            get => (string)GetValue(PlaceholderProperty);
            set => SetValue(PlaceholderProperty, value);
        }


        public static readonly BindableProperty TextAlignmentProperty =
            BindableProperty.Create(propertyName: nameof(NoBorderEditor), returnType: typeof(string),
                declaringType: typeof(string), defaultValue: string.Empty);

        public string TextAlignment
        {
            get => (string)GetValue(TextAlignmentProperty);
            set => SetValue(TextAlignmentProperty, value);
        }
    }
}
