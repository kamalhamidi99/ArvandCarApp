﻿using System;
using System.Linq;
using ArvandCarApp.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.Vacation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VacationDetailsView
    {
        private readonly long _vacationId;
        private readonly VacationDetailsViewModel _viewModel;
        private bool _loadAgain;

        public VacationDetailsView(long vacationId)
        {
            InitializeComponent();

            _vacationId = vacationId;
            _viewModel = new VacationDetailsViewModel(_vacationId);
            BindingContext = _viewModel;

            #region BackPageButton

            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) => await Navigation.PopAsync();
            BackPageButton.GestureRecognizers.Add(back);

            #endregion

            _loadAgain = false;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            if (_loadAgain)
            {
                Background.IsVisible = true;
                BackgroundLoading.IsVisible = true;

                await App.VacationItemService.LoadAsync();
                await _viewModel.LoadMoreData();

                Background.IsVisible = false;
                BackgroundLoading.IsVisible = false;
            }

            _loadAgain = false;
        }

        private void Button_VacationInfo(object sender, EventArgs e)
        {
            VacationInfoLine.IsVisible = true;
            DataInfoVacation.IsVisible = true;
            PaymentInfoLine.IsVisible = false;
            NoPayment.IsVisible = false;
            DataInfoPayment.IsVisible = false;
        }

        private void Button_PaymentInfo(object sender, EventArgs e)
        {
            VacationInfoLine.IsVisible = false;
            DataInfoVacation.IsVisible = false;
            PaymentInfoLine.IsVisible = true;
            NoPayment.IsVisible = _viewModel.NoPayment;
            DataInfoPayment.IsVisible = _viewModel.DataInfoPayment;
        }

        private async void PayButton_OnClicked(object sender, EventArgs e)
        {
            var data = (await App.VacationItemService.LoadAsync()).FirstOrDefault(x => x.Id == _vacationId);
            var pay3456 = data?.PaymonyList.FirstOrDefault(x => x.SetPayTypeActionSetPay == "3456");
            if (pay3456 != null)
            {
                _loadAgain = true;

                var login = await App.LoginService.GetAsync();
                var plainTextBytes =
                    System.Text.Encoding.UTF8.GetBytes(login.Username + "-" + login.Password + "-" + pay3456.Id);
                Device.OpenUri(new Uri(ApiUrlHelper.Url + "/WebService/MobileVacation/Payment?q=" +
                                       Convert.ToBase64String(plainTextBytes)));
                return;
            }

            await DisplayAlert("خطا", "امکان پرداخت وجود ندارد.", "بستن");
        }

    }
}