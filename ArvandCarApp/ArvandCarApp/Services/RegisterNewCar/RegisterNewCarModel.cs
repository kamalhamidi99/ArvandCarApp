﻿using SQLite;

namespace ArvandCarApp.Services.RegisterNewCar
{
    public class RegisterNewCarModel
    {
        [PrimaryKey]
        public byte Id { get; set; }

        public string Ishaghighi { get; set; }
        public string IsofficGoverMent { get; set; }
        public string OwnerMeliCode { get; set; }
        public string OwnerName { get; set; }
        public string OwnerFather { get; set; }
        public string OwnerShenasname { get; set; }
        public string OwnerSadere { get; set; }
        public string OwnerBirthdayStrYear { get; set; }
        public string OwnerBirthdayStrMonth { get; set; }
        public string OwnerBirthdayStrDay { get; set; }
        public string OwnerSex { get; set; }
        public string OwnerTell { get; set; }
        public string OwnerMobile { get; set; }
        public string OwnerAddress { get; set; }
        public string FkCarOption { get; set; }
        public string IhavePelakNumber { get; set; }
        public string FkCarNAme { get; set; }
        public string FkCarTip { get; set; }
        public string FkCarSystem { get; set; }
        public string CarYearCreate { get; set; }
        public string CarYearModle { get; set; }
        public string FkCarCountry { get; set; }
        public string FkCarColour { get; set; }
        public string CarDoorCounts { get; set; }
        public string CarSeatsCounts { get; set; }
        public string CarMotorCode { get; set; }
        public string CarShassisNumber { get; set; }
        public string CarCountryType { get; set; }
        public string CarTrasiteType { get; set; }
    }
}
