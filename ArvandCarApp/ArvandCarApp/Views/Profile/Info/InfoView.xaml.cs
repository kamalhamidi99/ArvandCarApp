﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.Info
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InfoView
    {
        public InfoView()
        {
            InitializeComponent();

            #region BackPageButton

            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) => await Navigation.PopAsync();
            BackPageButton.GestureRecognizers.Add(back);

            #endregion

            #region RefreshButton

            var refresh = new TapGestureRecognizer();
            refresh.Tapped += async (s, e) => await RefreshButton_Clicked();
            RefreshButton.GestureRecognizers.Add(refresh);

            #endregion
        }

        #region RefreshButton

        private async Task RefreshButton_Clicked()
        {
            Background.IsVisible = true;
            BackgroundLoading.IsVisible = true;

            var data = await App.FullInfoService.LoadAsync();
            InfoBoxItem.ItemsSource = new List<object>()
            {
                new
                {
                    Key = "نام مالک",
                    Value = data.OwnerName,
                },
                new
                {
                    Key = "کد ملی",
                    Value = data.OwnerMeliCode,
                },
                new
                {
                    Key = "نام پدر",
                    Value = data.OwnerFather,
                },
                new
                {
                    Key = "شماره شناسنامه",
                    Value = data.OwnerShenasname,
                },
                new
                {
                    Key = "محل صدور",
                    Value = data.OwnerSadere,
                },
                new
                {
                    Key = "تاریخ تولد",
                    Value = data.OwnerBirthdayStr,
                },
                new
                {
                    Key = "شماره تلفن",
                    Value = data.OwnerTell,
                },
                new
                {
                    Key = "شماره موبایل",
                    Value = data.OwnerMobile,
                },
                new
                {
                    Key = "جنسیت",
                    Value = data.OwnerSex,
                },
                new
                {
                    Key = "آدرس",
                    Value = data.OwnerAddress,
                },
                new
                {
                    Key = "نام خودرو",
                    Value = data.CarNameTitle,
                },
                new
                {
                    Key = "تیپ",
                    Value = data.CarTipTitle,
                },
                new
                {
                    Key = "سیستم",
                    Value = data.CarSystemTitle,
                },
                new
                {
                    Key = "سال ساخت",
                    Value = data.CarYearCreate,
                },
                new
                {
                    Key = "کشور سازنده",
                    Value = data.CarCountryTitle,
                },
                new
                {
                    Key = "پلاک",
                    Value = data.OwnerCarPelak,
                },
                new
                {
                    Key = "شماره شاسی",
                    Value = data.CarShassisNumber,
                },
                new
                {
                    Key = "شماره موتور",
                    Value = data.CarMotorCode,
                },
                new
                {
                    Key = "رنگ",
                    Value = data.CarColourTitle,
                },
                new
                {
                    Key = "تعداد درب",
                    Value = data.CarDoorCounts,
                },
                new
                {
                    Key = "تعداد سرنشین",
                    Value = data.CarSeatsCounts,
                },
                new
                {
                    Key = "نوع واردات",
                    Value = data.CarTrasiteType,
                },
                new
                {
                    Key = "حوزه ساخت",
                    Value = data.CarCountryType,
                },
            };

            Background.IsVisible = false;
            BackgroundLoading.IsVisible = false;
        }

        #endregion

    }
}