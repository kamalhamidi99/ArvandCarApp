﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ArvandCarApp.Helpers;
using ArvandCarApp.Models;
using Newtonsoft.Json;

namespace ArvandCarApp.Views.Profile.RecentClearance
{
    public class RecentClearanceViewModel : INotifyPropertyChanged
    {
        #region Fields

        private bool _isBusy;
        private IList<RecentClearanceModel> _allRecentClearanceModels;

        #endregion

        #region Properties

        private ObservableCollection<RecentClearanceModel> _recentClearanceModels;
        public ObservableCollection<RecentClearanceModel> RecentClearanceModels
        {
            get => _recentClearanceModels;
            set
            {
                _recentClearanceModels = value;
                OnPropertyChanged("RecentClearanceModels");
            }
        }

        private bool _loadingWait;
        public bool LoadingWait
        {
            get => _loadingWait;
            set
            {
                _loadingWait = value;
                OnPropertyChanged("LoadingWait");
            }
        }

        private bool _noData;
        public bool NoData
        {
            get => _noData;
            set
            {
                _noData = value;
                OnPropertyChanged("NoData");
            }
        }

        private bool _searchForm;
        public bool SearchForm
        {
            get => _searchForm;
            set
            {
                _searchForm = value;
                OnPropertyChanged("SearchForm");
            }
        }

        #endregion

        #region Constructor

        public RecentClearanceViewModel()
        {
            _recentClearanceModels = new ObservableCollection<RecentClearanceModel>();
            LoadMoreData();
        }

        public async Task LoadMoreData()
        {
            _isBusy = false;
            _loadingWait = true;
            _noData = false;
            _searchForm = false;

            await Task.Delay(300);

            //To ignore if items are being added till delayed time.
            if (_isBusy)
                return;

            var httpCarNames = await HttpClientHelper.SendAndReadAsync(HttpMethod.Post,
                $"{ApiUrlHelper.Url}/api/RecentClearance", isAuthorization: true);
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                var desc = JsonConvert.DeserializeObject<ObservableCollection<RecentClearanceModel>>(httpCarNames.ResponseMessage);
                _recentClearanceModels = desc;
                OnPropertyChanged("RecentClearanceModels");

                _searchForm = true;
                OnPropertyChanged("SearchForm");
            }
            else
            {
                _noData = true;
                OnPropertyChanged("NoData");
            }

            _loadingWait = false;
            OnPropertyChanged("LoadingWait");

            _isBusy = true;
        }

        #endregion

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #endregion

    }
}
