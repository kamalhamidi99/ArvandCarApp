﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ArvandCarApp.Helpers;
using Newtonsoft.Json;

namespace ArvandCarApp.Services.LeftZone.LeftZoneInfo
{
    public class LeftZoneInfoService
    {

        public async Task<LeftZoneInfoModel> LoadAsync()
        {
            var httpCarNames =
                await HttpClientHelper.SendAndReadAsync(HttpMethod.Post, $"{ApiUrlHelper.Url}/api/leftzone/info", isAuthorization: true);
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrWhiteSpace(httpCarNames.ResponseMessage))
                {
                    var data = JsonConvert.DeserializeObject<LeftZoneInfoModel>(httpCarNames.ResponseMessage);
                    if (data != null)
                    {
                        return data;
                    }
                }
            }

            return new LeftZoneInfoModel();
        }

    }
}
