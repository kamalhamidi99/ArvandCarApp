﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using ArvandCarApp.Models;

namespace ArvandCarApp.Views.Profile.Info
{
    public class InfoViewModel : INotifyPropertyChanged
    {
        #region Fields

        private bool _isBusy;
        private IList<KeyValueDto> _allInfoModels;

        #endregion

        #region Properties

        private ObservableCollection<KeyValueDto> _infoModels;

        public ObservableCollection<KeyValueDto> InfoModels
        {
            get => _infoModels;
            set
            {
                _infoModels = value;
                OnPropertyChanged("InfoModels");
            }
        }

        private bool _loadingWait;
        public bool LoadingWait
        {
            get => _loadingWait;
            set
            {
                _loadingWait = value;
                OnPropertyChanged("LoadingWait");
            }
        }

        #endregion

        #region Constructor

        public InfoViewModel()
        {
            _infoModels = new ObservableCollection<KeyValueDto>();
            LoadMoreData();
        }

        public async Task LoadMoreData()
        {
            _isBusy = false;
            _loadingWait = true;

            await Task.Delay(300);

            //To ignore if items are being added till delayed time.
            if (_isBusy)
                return;

             var data = await App.FullInfoService.GetAsync();
            if (data != null && data.Id > 0)
            {
                _allInfoModels = new List<KeyValueDto>()
                {
                    new KeyValueDto
                    {
                        Key = "نام مالک",
                        Value = data.OwnerName,
                    },
                    new KeyValueDto
                    {
                        Key = "کد ملی",
                        Value = data.OwnerMeliCode,
                    },
                    new KeyValueDto
                    {
                        Key = "نام پدر",
                        Value = data.OwnerFather,
                    },
                    new KeyValueDto
                    {
                        Key = "شماره شناسنامه",
                        Value = data.OwnerShenasname,
                    },
                    new KeyValueDto
                    {
                        Key = "محل صدور",
                        Value = data.OwnerSadere,
                    },
                    new KeyValueDto
                    {
                        Key = "تاریخ تولد",
                        Value = data.OwnerBirthdayStr,
                    },
                    new KeyValueDto
                    {
                        Key = "شماره تلفن",
                        Value = data.OwnerTell,
                    },
                    new KeyValueDto
                    {
                        Key = "شماره موبایل",
                        Value = data.OwnerMobile,
                    },
                    new KeyValueDto
                    {
                        Key = "جنسیت",
                        Value = data.OwnerSex,
                    },
                    new KeyValueDto
                    {
                        Key = "آدرس",
                        Value = data.OwnerAddress,
                    },
                    new KeyValueDto
                    {
                        Key = "نام خودرو",
                        Value = data.CarNameTitle,
                    },
                    new KeyValueDto
                    {
                        Key = "تیپ",
                        Value = data.CarTipTitle,
                    },
                    new KeyValueDto
                    {
                        Key = "سیستم",
                        Value = data.CarSystemTitle,
                    },
                    new KeyValueDto
                    {
                        Key = "سال ساخت",
                        Value = data.CarYearCreate,
                    },
                    new KeyValueDto
                    {
                        Key = "کشور سازنده",
                        Value = data.CarCountryTitle,
                    },
                    new KeyValueDto
                    {
                        Key = "پلاک",
                        Value = data.OwnerCarPelak,
                    },
                    new KeyValueDto
                    {
                        Key = "شماره شاسی",
                        Value = data.CarShassisNumber,
                    },
                    new KeyValueDto
                    {
                        Key = "شماره موتور",
                        Value = data.CarMotorCode,
                    },
                    new KeyValueDto
                    {
                        Key = "رنگ",
                        Value = data.CarColourTitle,
                    },
                    new KeyValueDto
                    {
                        Key = "تعداد درب",
                        Value = data.CarDoorCounts,
                    },
                    new KeyValueDto
                    {
                        Key = "تعداد سرنشین",
                        Value = data.CarSeatsCounts,
                    },
                    new KeyValueDto
                    {
                        Key = "نوع واردات",
                        Value = data.CarTrasiteType,
                    },
                    new KeyValueDto
                    {
                        Key = "حوزه ساخت",
                        Value = data.CarCountryType,
                    },
                };
            }

            if (_allInfoModels == null || !_allInfoModels.Any())
                return;

            foreach (var model in _allInfoModels)
            {
                _infoModels.Add(model);
            }

            _loadingWait = false;
            OnPropertyChanged("LoadingWait");

            _isBusy = true;
        }

        #endregion

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #endregion

    }
}
