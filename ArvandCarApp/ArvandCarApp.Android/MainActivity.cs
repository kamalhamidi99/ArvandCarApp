﻿using Android.App;
using Android.Content.PM;
using Android.Net;
using Android.OS;

namespace ArvandCarApp.Droid
{
    [Activity(Label = "Arvand Car", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.Orientation)]
    public class MainActivity : Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static ConnectivityManager ConnectivityManager { get; set; }

        protected override void OnCreate(Bundle bundle)
        {
            ConnectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            //// create database
            //var sqLite = new SqLiteService();
            //sqLite.DbConnection();

            Xamarin.Forms.Forms.Init(this, bundle);
            //AndroidBug5497WorkaroundForXamarinAndroid.assistActivity(this);

            LoadApplication(new App());

            //XFGloss.Droid.Library.Init(this, bundle);

        }
    }
}

