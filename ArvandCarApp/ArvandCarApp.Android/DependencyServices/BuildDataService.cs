using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Droid.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(BuildDataService))]
namespace ArvandCarApp.Droid.DependencyServices
{
    public class BuildDataService : IBuildDataService
    {
        public string Platform => "Android";

        public string VersionCode
        {
            get
            {
                var context = Forms.Context;
                return context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionCode.ToString();
            }
        }

        public string VersionName
        {
            get
            {
                var context = Forms.Context;
                return context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
            }
        }
    }
}