﻿using Android.Content;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Droid.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(CafeBazarService))]
namespace ArvandCarApp.Droid.DependencyServices
{
    public class CafeBazarService : ICafeBazarService
    {
        public string SendMessage()
        {
            try
            {
                var intent = new Intent(Intent.ActionEdit);
                intent.SetData(Android.Net.Uri.Parse("bazaar://details?id=com.arvand.car"));
                intent.SetPackage("com.farsitel.bazaar");
                Forms.Context.StartActivity(intent);
                return string.Empty;
            }
            catch
            {
                return "خطایی رخ داد";
            }
        }
    }
}