﻿using System.Collections.Generic;

namespace ArvandCarApp.Models.Api
{
    class ApiBaseDto
    {
        public IEnumerable<ApiBaseDataDto> CarName { get; set; }
        public IEnumerable<ApiBaseDataDto> Color { get; set; }
        public IEnumerable<ApiBaseDataDto> ManufacturingCountry { get; set; }
        public IEnumerable<ApiBaseDataDto> System { get; set; }
        public IEnumerable<ApiBaseDataDto> AreaBuild { get; set; }
        public IEnumerable<ApiBaseDataDto> Trans { get; set; }
        public IEnumerable<int> YearOfConstruction { get; set; }
        public IEnumerable<int> CarDoor { get; set; }
        public IEnumerable<int> CarSeat { get; set; }
    }
}
