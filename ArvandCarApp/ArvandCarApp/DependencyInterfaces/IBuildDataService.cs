﻿namespace ArvandCarApp.DependencyInterfaces
{
    public interface IBuildDataService
    {
        string Platform { get; }
        string VersionCode { get; }
        string VersionName { get; }
    }
}
