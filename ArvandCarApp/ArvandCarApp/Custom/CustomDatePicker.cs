﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ArvandCarApp.DependencyInterfaces;
using Syncfusion.SfPicker.XForms;
using Xamarin.Forms;

namespace ArvandCarApp.Custom
{
    public class CustomDatePicker : SfPicker
    {
        public Dictionary<string, string> Months;
        public ObservableCollection<object> Date { get; set; }

        public ObservableCollection<object> Day;
        public ObservableCollection<object> Month;
        public ObservableCollection<object> Year;

        public ObservableCollection<object> Time { get; set; }
        public ObservableCollection<object> Minute;
        public ObservableCollection<object> Hour;
        public ObservableCollection<object> Format;

        public ObservableCollection<string> Headers { get; set; }
        public CustomDatePicker()
        {
            Months = new Dictionary<string, string>();

            Date = new ObservableCollection<object>();
            Day = new ObservableCollection<object>();
            Month = new ObservableCollection<object>();
            Year = new ObservableCollection<object>();
            Headers = new ObservableCollection<string> {"سال", "ماه", "روز"};
            PopulateDateCollection();
            ItemsSource = Date;
            ColumnHeaderText = Headers;

            SelectionChanged += CustomDatePicker_SelectionChanged;
        }

        private void CustomDatePicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //UpdateDays(Date, e);
        }

        private void PopulateDateCollection()
        {
            var dateService = DependencyService.Get<IPersianDateService>();

            //populate months
            Month = new ObservableCollection<object>(dateService.GetPersianMonthName().Select(x => x.Value).ToList());

            //populate year
            var currentYear = dateService.GetPersianDateArray();
            for (int i = 1300; i <= int.Parse(currentYear[0]); i++)
            {
                Year.Add(i.ToString());
            }

            //populate Days
            for (int i = 1; i <= 31; i++)
            {
                if (i < 10)
                {
                    Day.Add("0" + i);
                }
                else
                    Day.Add(i.ToString());
            }

            Date.Add(Year);
            Date.Add(Month);
            Date.Add(Day);
        }

    }
}
