﻿using System.Collections.Generic;
using ArvandCarApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.LicensedCompany
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LicensedCompanyView 
    {
        public LicensedCompanyView()
        {
            InitializeComponent();

            #region BackPageButton

            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) => await Navigation.PopAsync();
            BackPageButton.GestureRecognizers.Add(back);

            #endregion

            LicensedCompanyItems.ItemsSource = new List<LicensedCompanyModel>()
            {
                new LicensedCompanyModel
                {
                    Name = "آرمان خودروی اروند",
                },
                new LicensedCompanyModel
                {
                    Name = "زیبا خودرو اروند",
                },
                new LicensedCompanyModel
                {
                    Name = "گلرنگ موتور اروند",
                },
            };
        }
    }
}