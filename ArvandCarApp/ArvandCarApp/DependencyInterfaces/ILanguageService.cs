﻿namespace ArvandCarApp.DependencyInterfaces
{
    public interface ILanguageService
    {
        string Language { get; set; }
        string Country { get; set; }
    }
}