﻿using SQLite;

namespace ArvandCarApp.Services.FullInfo
{
    public class FullInfoModel
    {
        [PrimaryKey]
        public int Id { get; set; }

        /// <summary>
        /// نام خودرو
        /// </summary>
        public string CarNameTitle { get; set; }
        /// <summary>
        /// سیستم
        /// </summary>
        public string CarSystemTitle { get; set; }
        /// <summary>
        /// تیپ
        /// </summary>
        public string CarTipTitle { get; set; }
        /// <summary>
        /// سال ساخت
        /// </summary>
        public string CarYearCreate { get; set; }
        /// <summary>
        /// مدل ساخت
        /// </summary>
        public string CarYearModel { get; set; }
        /// <summary>
        /// کشور سازنده
        /// </summary>
        public string CarCountryTitle { get; set; }
        /// <summary>
        /// شماره موتور
        /// </summary>
        public string CarMotorCode { get; set; }
        /// <summary>
        /// رنگ
        /// </summary>
        public string CarColourTitle { get; set; }
        /// <summary>
        /// شماره شاسی
        /// </summary>
        public string CarShassisNumber { get; set; }
        /// <summary>
        /// تعداد درها
        /// </summary>
        public string CarDoorCounts { get; set; }
        /// <summary>
        /// تعداد سرنشین
        /// </summary>
        public string CarSeatsCounts { get; set; }
        /// <summary>
        /// حوزه ساخت
        /// </summary>
        public string CarCountryType { get; set; }
        /// <summary>
        /// نوع واردات
        /// </summary>
        public string CarTrasiteType { get; set; }
        /// <summary>
        /// آپشن
        /// </summary>
        public string CraOptionTitle { get; set; }
        /// <summary>
        /// پلاک
        /// </summary>
        public string OwnerCarPelak { get; set; }
        /// <summary>
        /// شناسه ملی شرکت-کد ملی مالک
        /// </summary>
        public string OwnerMeliCode { get; set; }
        /// <summary>
        /// نام شرکت/ارگان/سازمان-نام مالک
        /// </summary>
        public string OwnerName { get; set; }
        /// <summary>
        /// نام مدیر-نام پدر
        /// </summary>
        public string OwnerFather { get; set; }
        /// <summary>
        /// شماره ثبت-شماره شناسنامه
        /// </summary>
        public string OwnerShenasname { get; set; }
        /// <summary>
        /// محل ثبت-محل صدور
        /// </summary>
        public string OwnerSadere { get; set; }
        //[Display("","")]
        /// <summary>
        /// تاریخ تولد-تاریخ ثبت
        /// </summary>
        public string OwnerBirthdayStr { get; set; }
        /// <summary>
        /// شماره تماس
        /// </summary>
        public string OwnerTell { get; set; }
        /// <summary>
        /// شماره موبایل
        /// </summary>
        public string OwnerMobile { get; set; }
        /// <summary>
        /// نوع مالک
        /// </summary>
        public string IsHaghighi { get; set; }
        /// <summary>
        /// جنسیت
        /// </summary>
        public string OwnerSex { get; set; }
        /// <summary>
        /// آدرس
        /// </summary>
        public string OwnerAddress { get; set; }
        /// <summary>
        /// نام نماینده
        /// </summary>
        public string OwnerCarNamaiandehName { get; set; }
        /// <summary>
        /// کد ملی نماینده
        /// </summary>
        public string OwnerCarNamaiandehMeliCode { get; set; }
        /// <summary>
        /// دفتر وکالت
        /// </summary>
        public string VekaltDaftar { get; set; }
        /// <summary>
        /// شماره وکالت
        /// </summary>
        public string VekaltNumber { get; set; }
        /// <summary>
        /// تاریخ وکالت
        /// </summary>
        public string VekaltDateEndStr { get; set; }
        /// <summary>
        /// کد رهگیری
        /// </summary>
        public string CarRegisterCode { get; set; }
    }
}
