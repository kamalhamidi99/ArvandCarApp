﻿using System;
using ArvandCarApp.Views.Account.Login;
using Xamarin.Forms.Xaml;
using MainView = ArvandCarApp.Views.Profile.Main.MainView;

namespace ArvandCarApp.Views.Account.MobileNumber
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MobileNumberView
    {
        private readonly MobileNumberViewModel _viewModel;

        public MobileNumberView()
        {
            InitializeComponent();

            _viewModel = new MobileNumberViewModel();
            BindingContext = _viewModel;

            MobileNumberEntry.Text = ""; //09361260491
        }

        private async void SendButton_OnClicked(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(MobileNumberEntry.Text))
            {
                await DisplayAlert("خطا", "لطفا شماره موبایل را وارد کنید.", "بستن");
                return;
            }

            if (!CheckMobile(MobileNumberEntry.Text))
            {
                await DisplayAlert("خطا", "شماره موبایل وارد شده صحیج نمی باشد.", "بستن");
                return;
            }

            Background.IsVisible = true;
            BackgroundLoading.IsVisible = true;

            var send = await App.NumberValidationService.SendAsync(MobileNumberEntry.Text);
            if (!send)
            {
                await DisplayAlert("خطا", "امکان ذخیره اطلاعات وجود ندارد.", "بستن");
            }
            else
            {
                LabelValidateCode.Text = $"کد ورود به شماره {MobileNumberEntry.Text} ارسال شد.";
                SendForm.IsVisible = false;
                ContinueForm.IsVisible = true;
                _viewModel.RunTimer();
            }

            Background.IsVisible = false;
            BackgroundLoading.IsVisible = false;
        }

        private async void ContinueButton_OnClicked(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(ValidateCodeEntry.Text))
            {
                await DisplayAlert("خطا", "لطفا کد تایید را وارد کنید.", "بستن");
                return;
            }

            if (ValidateCodeEntry.Text.Length != 5)
            {
                await DisplayAlert("خطا", "کد تایید وارد شده صحیج نمی باشد.", "بستن");
                return;
            }

            Background.IsVisible = true;
            BackgroundLoading.IsVisible = true;

            var validatw = await App.NumberValidationService.ValidateAsync(ValidateCodeEntry.Text);
            if (!validatw)
            {
                var get = await App.NumberValidationService.GetAsync();
                if (get.ValidationCode != ValidateCodeEntry.Text)
                {
                    await DisplayAlert("خطا", "کد وارد شده صحیح نمی باشد.", "بستن");
                }
                else
                {
                    await DisplayAlert("خطا", "امکان ذخیره اطلاعات وجود ندارد.", "بستن");
                }
            }
            else
            {
                await App.FullInfoService.LoadAsync();

                Navigation.InsertPageBefore(new MainView(), Navigation.NavigationStack[0]);
                await Navigation.PopToRootAsync();
            }

            Background.IsVisible = false;
            BackgroundLoading.IsVisible = false;
        }

        private static bool CheckMobile(string number)
        {
            if (number.Trim().Length == 11)
            {
                if (number.Substring(0, 2) == "09")
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private async void LoginAgainTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            await App.NumberValidationService.DeleteAsync();
            await App.LoginService.DeleteAsync();

            Navigation.InsertPageBefore(new LoginView(), Navigation.NavigationStack[0]);
            await Navigation.PopToRootAsync();
        }

    }
}