﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using SQLite;

namespace ArvandCarApp.Services.Vacation.VacationPaymonyItem
{
    public class VacationPaymonyItemService
    {
        readonly SQLiteAsyncConnection _database;

        public VacationPaymonyItemService()
        {
            _database = App.SqLiteAsyncConnection;
        }

        public async Task StoreDataAsync(IList<VacationPaymonyItemModel> models, string fkparent)
        {
            try
            {
                if (await TableExistAsync())
                {
                    var allPays = await GetAllAsync(fkparent: fkparent);
                    if (allPays != null && allPays.Any())
                    {
                        foreach (var item in allPays)
                        {
                            await _database.DeleteAsync(item);
                        }
                    }
                }

                await _database.CreateTableAsync<VacationPaymonyItemModel>();
                await _database.InsertAllAsync(models);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }
        }

        public async Task DeleteAllAsync()
        {
            if (await TableExistAsync())
                await _database.DropTableAsync<VacationPaymonyItemModel>();
        }

        public async Task<IList<VacationPaymonyItemModel>> GetAllAsync(string fkparent = null)
        {
            if (!await TableExistAsync())
                return new List<VacationPaymonyItemModel>();

            var result = await _database.Table<VacationPaymonyItemModel>().ToListAsync();
            if (!string.IsNullOrWhiteSpace(fkparent))
            {
                result = result.Where(x => x.FkParant == fkparent).ToList();
            }

            return result;
        }

        private async Task<bool> TableExistAsync()
        {
            var tableExistsQuery = $"SELECT name FROM sqlite_master WHERE type='table' AND name='{nameof(VacationPaymonyItemModel)}';";
            var result = await _database.ExecuteScalarAsync<string>(tableExistsQuery);
            if (result != null)
                return true;

            return false;
        }

    }
}
