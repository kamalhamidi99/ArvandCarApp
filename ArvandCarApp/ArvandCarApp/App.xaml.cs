﻿using System;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Services.FullInfo;
using ArvandCarApp.Services.History;
using ArvandCarApp.Services.LeftZone.LeftZoneInfo;
using ArvandCarApp.Services.License;
using ArvandCarApp.Services.Login;
using ArvandCarApp.Services.NumberValidation;
using ArvandCarApp.Services.Vacation.VacationItem;
using ArvandCarApp.Services.Vacation.VacationPaymonyItem;
using ArvandCarApp.Views;
using ArvandCarApp.Views.Account.Login;
using ArvandCarApp.Views.Account.MobileNumber;
using ArvandCarApp.Views.Expire;
using ArvandCarApp.Views.Register;
using SQLite;
using Xamarin.Forms;
using MainView = ArvandCarApp.Views.Profile.Main.MainView;

namespace ArvandCarApp
{
    public partial class App : Application
    {
        #region SqLiteAsyncConnection

        private static SQLiteAsyncConnection _sqLiteAsyncConnection;
        public static SQLiteAsyncConnection SqLiteAsyncConnection => _sqLiteAsyncConnection ?? (_sqLiteAsyncConnection =
                                                                         new SQLiteAsyncConnection(
                                                                             DependencyService.Get<IFileService>()
                                                                                 .GetLocalFilePath(
                                                                                     $"{nameof(ArvandCarApp)}.db3")));

        #endregion

        #region Service

        static FullInfoService _fullInfoService;
        public static FullInfoService FullInfoService => _fullInfoService ?? (_fullInfoService = new FullInfoService());

        static HistoryService _historyService;
        public static HistoryService HistoryService => _historyService ?? (_historyService = new HistoryService());

        static VacationItemService _vacationItemService;
        public static VacationItemService VacationItemService =>
            _vacationItemService ?? (_vacationItemService = new VacationItemService());

        static VacationPaymonyItemService _vacationPaymonyItemService;
        public static VacationPaymonyItemService VacationPaymonyItemService =>
            _vacationPaymonyItemService ?? (_vacationPaymonyItemService = new VacationPaymonyItemService());

        static LicenseService _licenseService;
        public static LicenseService LicenseService =>
            _licenseService ?? (_licenseService = new LicenseService());

        static LeftZoneInfoService _leftZoneInfoService;
        public static LeftZoneInfoService LeftZoneInfoService =>
            _leftZoneInfoService ?? (_leftZoneInfoService = new LeftZoneInfoService());

        static NumberValidationService _numberValidationService;
        public static NumberValidationService NumberValidationService =>
            _numberValidationService ?? (_numberValidationService = new NumberValidationService());

        static LoginService _loginService;
        public static LoginService LoginService => _loginService ?? (_loginService = new LoginService());

        #endregion

        #region Property

        internal static string SmsMobileNumberValidate;

        internal static bool VacationLoadDataIfEmpty = true;
        internal static bool VacationDetailsPullToRefresh = false;

        #endregion

        #region App

        public App()
        {
            var networkInfo = DependencyService.Get<INetworkService>();
            if (networkInfo != null && networkInfo.IsOnline())
            {
                //if (DateTime.Now > new DateTime(2019, 1, 21))
                //{
                //    MainPage = new NavigationPage(new ExpireView());
                //}
                //else
                //{
                    if (LoginService.IsLogin())
                    {
                        if (NumberValidationService.IsValidate())
                        {
                            MainPage = new NavigationPage(new MainView());
                        }
                        else
                        {
                            MainPage = new NavigationPage(new MobileNumberView());
                        }
                    }
                    else
                    {
                        MainPage = new NavigationPage(new LoginView());
                    }
                //}
            }
            else
            {
                MainPage = new NavigationPage(new NoNetworkView());
            }
        }

        #endregion

        #region On

        protected override void OnStart()
        {
            //Handle when your app starts
            //AppCenter.Start("android=832d5a7f-dd51-4702-acb4-1477fd1e6efe;" + "uwp={Your UWP App secret here};" +
            //                "ios={Your iOS App secret here}",
            //    typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        #endregion
    }
}
