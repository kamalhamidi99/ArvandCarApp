﻿using System.Threading.Tasks;
using ArvandCarApp.Services.LeftZone.LeftZoneInfo;
using ArvandCarApp.ViewModels;
using Xamarin.Forms;

namespace ArvandCarApp.Views.Profile.LeftZone
{
    public class LeftZoneMainViewModel : BaseViewModel
    {
        #region Properties

        public Command LoadItemsCommand { get; set; }

        private bool _loadingWait;
        public bool LoadingWait
        {
            get => _loadingWait;
            set => SetProperty(ref _loadingWait, value);
        }

        private string _leftZoneCalculateTotalDay;
        public string LeftZoneCalculateTotalDay
        {
            get => _leftZoneCalculateTotalDay;
            set => SetProperty(ref _leftZoneCalculateTotalDay, value);
        }

        private string _leftZoneCalculateTotalDayInMorakhasi;
        public string LeftZoneCalculateTotalDayInMorakhasi
        {
            get => _leftZoneCalculateTotalDayInMorakhasi;
            set => SetProperty(ref _leftZoneCalculateTotalDayInMorakhasi, value);
        }

        private string _leftZoneCalculateTotalDayPaied;
        public string LeftZoneCalculateTotalDayPaied
        {
            get => _leftZoneCalculateTotalDayPaied;
            set => SetProperty(ref _leftZoneCalculateTotalDayPaied, value);
        }

        private string _leftZoneCalculateTotalPrice;
        public string LeftZoneCalculateTotalPrice
        {
            get => _leftZoneCalculateTotalPrice;
            set => SetProperty(ref _leftZoneCalculateTotalPrice, value);
        }

        private string _leftZonePaymentYearlyFixAmountAmountPaied;
        public string LeftZonePaymentYearlyFixAmountAmountPaied
        {
            get => _leftZonePaymentYearlyFixAmountAmountPaied;
            set => SetProperty(ref _leftZonePaymentYearlyFixAmountAmountPaied, value);
        }

        private string _leftZonePaymentYearlyFixAmountForPersianYear;
        public string LeftZonePaymentYearlyFixAmountForPersianYear
        {
            get => _leftZonePaymentYearlyFixAmountForPersianYear;
            set => SetProperty(ref _leftZonePaymentYearlyFixAmountForPersianYear, value);
        }

        private bool _leftZoneFreeIsVisible;
        public bool LeftZoneFreeIsVisible
        {
            get => _leftZoneFreeIsVisible;
            set => SetProperty(ref _leftZoneFreeIsVisible, value);
        }

        private bool _leftZoneGpsErrorIsVisible;
        public bool LeftZoneGpsErrorIsVisible
        {
            get => _leftZoneGpsErrorIsVisible;
            set => SetProperty(ref _leftZoneGpsErrorIsVisible, value);
        }

        private bool _leftZoneYearlyAmountFixIsVisible;
        public bool LeftZoneYearlyAmountFixIsVisible
        {
            get => _leftZoneYearlyAmountFixIsVisible;
            set => SetProperty(ref _leftZoneYearlyAmountFixIsVisible, value);
        }

        private bool _leftZoneOldYearNotPayIsVisible;
        public bool LeftZoneOldYearNotPayIsVisible
        {
            get => _leftZoneOldYearNotPayIsVisible;
            set => SetProperty(ref _leftZoneOldYearNotPayIsVisible, value);
        }

        private bool _leftZoneNormalIsVisible;
        public bool LeftZoneNormalIsVisible
        {
            get => _leftZoneNormalIsVisible;
            set => SetProperty(ref _leftZoneNormalIsVisible, value);
        }

        private bool _detailsBoxIsVisible;
        public bool DetailsBoxIsVisible
        {
            get => _detailsBoxIsVisible;
            set => SetProperty(ref _detailsBoxIsVisible, value);
        }

        private Color _detailsBoxBackgroundColor;
        public Color DetailsBoxBackgroundColor
        {
            get => _detailsBoxBackgroundColor;
            set => SetProperty(ref _detailsBoxBackgroundColor, value);
        }

        private bool _stackLayoutYearlyFixAmountForPersianYearIsVisible;
        public bool StackLayoutYearlyFixAmountForPersianYearIsVisible
        {
            get => _stackLayoutYearlyFixAmountForPersianYearIsVisible;
            set => SetProperty(ref _stackLayoutYearlyFixAmountForPersianYearIsVisible, value);
        }

        private bool _stackLayoutYearlyFixAmountAmountPaiedIsVisible;
        public bool StackLayoutYearlyFixAmountAmountPaiedIsVisible
        {
            get => _stackLayoutYearlyFixAmountAmountPaiedIsVisible;
            set => SetProperty(ref _stackLayoutYearlyFixAmountAmountPaiedIsVisible, value);
        }

        private bool _stackLayoutTotalDayIsVisible;
        public bool StackLayoutTotalDayIsVisible
        {
            get => _stackLayoutTotalDayIsVisible;
            set => SetProperty(ref _stackLayoutTotalDayIsVisible, value);
        }

        private bool _stackLayoutTotalPriceIsVisible;
        public bool StackLayoutTotalPriceIsVisible
        {
            get => _stackLayoutTotalPriceIsVisible;
            set => SetProperty(ref _stackLayoutTotalPriceIsVisible, value);
        }

        private bool _stackLayoutTotalDayPaiedIsVisible;
        public bool StackLayoutTotalDayPaiedIsVisible
        {
            get => _stackLayoutTotalDayPaiedIsVisible;
            set => SetProperty(ref _stackLayoutTotalDayPaiedIsVisible, value);
        }

        private bool _stackLayoutTotalDayInMorakhasiIsVisible;
        public bool StackLayoutTotalDayInMorakhasiIsVisible
        {
            get => _stackLayoutTotalDayInMorakhasiIsVisible;
            set => SetProperty(ref _stackLayoutTotalDayInMorakhasiIsVisible, value);
        }

        private bool _showPaiedIsVisible;
        public bool ShowPaiedIsVisible
        {
            get => _showPaiedIsVisible;
            set => SetProperty(ref _showPaiedIsVisible, value);
        }

        private bool _showPayingIsVisible;
        public bool ShowPayingIsVisible
        {
            get => _showPayingIsVisible;
            set => SetProperty(ref _showPayingIsVisible, value);
        }

        #endregion

        #region Constructor

        public LeftZoneMainViewModel()
        {
            LoadItemsCommand = new Command(async () => await LoadMoreData());
        }

        public async Task LoadMoreData()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            LoadingWait = true;

            await Task.Delay(100);

            var model = await App.LeftZoneInfoService.LoadAsync();

            LeftZoneCalculateTotalDay = model.LeftZoneCalculateTotalDay.ToString();
            LeftZoneCalculateTotalDayInMorakhasi = model.LeftZoneCalculateTotalDayInMorakhasi.ToString();
            LeftZoneCalculateTotalDayPaied = model.LeftZoneCalculateTotalDayPaied.ToString();
            LeftZoneCalculateTotalPrice = model.LeftZoneCalculateTotalPrice.ToString();
            LeftZonePaymentYearlyFixAmountAmountPaied = model.LeftZonePaymentYearlyFixAmountAmountPaied.ToString();
            LeftZonePaymentYearlyFixAmountForPersianYear = model.LeftZonePaymentYearlyFixAmountForPersianYear.ToString();


            LeftZoneFreeIsVisible = false;
            LeftZoneGpsErrorIsVisible = false;
            LeftZoneYearlyAmountFixIsVisible = false;
            LeftZoneOldYearNotPayIsVisible = false;
            LeftZoneNormalIsVisible = false;
            DetailsBoxIsVisible = false;
            StackLayoutYearlyFixAmountForPersianYearIsVisible = false;
            StackLayoutYearlyFixAmountAmountPaiedIsVisible = false;
            StackLayoutTotalDayIsVisible = false;
            StackLayoutTotalPriceIsVisible = false;
            StackLayoutTotalDayPaiedIsVisible = false;
            StackLayoutTotalDayInMorakhasiIsVisible = false;

            if (model.LeftZoneGroupCarType == LeftZoneGroupCarType.Free)
            {
                LeftZoneFreeIsVisible = true;
            }
            if (model.LeftZoneGroupCarType == LeftZoneGroupCarType.GpsError)
            {
                LeftZoneGpsErrorIsVisible = true;
            }
            if (model.LeftZoneGroupCarType == LeftZoneGroupCarType.YearlyFixAmount)
            {
                LeftZoneYearlyAmountFixIsVisible = true;
                if (model.LeftZoneGroupCarPaymentOldYearNotPay) _leftZoneOldYearNotPayIsVisible = true;
                DetailsBoxIsVisible = true;
                StackLayoutYearlyFixAmountForPersianYearIsVisible = true;
                StackLayoutYearlyFixAmountAmountPaiedIsVisible = true;
            }
            if (model.LeftZoneGroupCarType == LeftZoneGroupCarType.Normal)
            {
                LeftZoneNormalIsVisible = true;
                DetailsBoxIsVisible = true;
                StackLayoutTotalDayIsVisible = true;
                StackLayoutTotalPriceIsVisible = true;
                StackLayoutTotalDayPaiedIsVisible = true;
                StackLayoutTotalDayInMorakhasiIsVisible = true;
            }
            if (model.LeftZoneGroupCarType == LeftZoneGroupCarType.Normal ||
                model.LeftZoneGroupCarType == LeftZoneGroupCarType.YearlyFixAmount)
            {
                DetailsBoxBackgroundColor = model.LeftZoneCalculateTotalPrice <= 0 ? Color.DarkCyan : Color.DarkRed;
                ShowPaiedIsVisible = model.LeftZoneCalculateTotalPrice <= 0;
                ShowPayingIsVisible = !(model.LeftZoneCalculateTotalPrice <= 0);
            }

            LoadingWait = false;
            IsBusy = true;
        }

        #endregion

    }
}
