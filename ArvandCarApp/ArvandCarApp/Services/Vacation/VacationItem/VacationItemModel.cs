﻿using System;
using System.Collections.Generic;
using ArvandCarApp.Services.Vacation.VacationPaymonyItem;
using SQLite;

namespace ArvandCarApp.Services.Vacation.VacationItem
{
    public class VacationItemModel
    {
        [PrimaryKey]
        public long Id { get; set; }
        public string Fkcar { get; set; }
        public string IsOk { get; set; }
        public string IsOkTitle { get; set; }
        public string IsPay { get; set; }
        public string IsPayTitle { get; set; }
        public string GpsCheckIsOk { get; set; }
        public string GpsCheckIsOkTitle { get; set; }
        public string Isdomorkhasi { get; set; }
        public string IsdomorkhasiTitle { get; set; }
        public string MorakhasiPelakType { get; set; }
        public string MorakhasiPelakTypeTitle { get; set; }
        public string MorakhasiDayType { get; set; }
        public string MorakhasiDayTypeTitle { get; set; }
        public string MorakhasiTypeTitle { get; set; }
        public string MorakhasiComeOnMsg { get; set; }
        public string MorakhasidateRegisterStr { get; set; }
        public string MorakhasiDateStartStr { get; set; }
        public DateTime MorakhasiDateStart { get; set; }
        public string MorakhasiDateEndStr { get; set; }
        public DateTime MorakhasiDateEnd { get; set; }
        public string CanPay { get; set; }
        [Ignore]
        public IList<VacationPaymonyItemModel> PaymonyList { get; set; }
        [Ignore]
        public string Color { get; set; }
        [Ignore]
        public int IsOkInt { get; set; }
        [Ignore]
        public bool ShowPaying { get; set; }
        [Ignore]
        public bool ShowPaied { get; set; }

    }
}
