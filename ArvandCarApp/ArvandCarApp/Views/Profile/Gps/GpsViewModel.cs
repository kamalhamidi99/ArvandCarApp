﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ArvandCarApp.Helpers;
using ArvandCarApp.Models;
using ArvandCarApp.ViewModels;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace ArvandCarApp.Views.Profile.Gps
{
    public class GpsViewModel : BaseViewModel
    {
        #region Properties

        public Command LoadItemsCommand { get; set; }

        private string _gpsState;
        public string GpsState
        {
            get => _gpsState;
            set => SetProperty(ref _gpsState, value);
        }

        private string _gpsStateImage;
        public string GpsStateImage
        {
            get => _gpsStateImage;
            set => SetProperty(ref _gpsStateImage, value);
        }

        private string _gpsMessage;
        public string GpsMessage
        {
            get => _gpsMessage;
            set => SetProperty(ref _gpsMessage, value);
        }

        private bool _loadingWait;
        public bool LoadingWait
        {
            get => _loadingWait;
            set => SetProperty(ref _loadingWait, value);
        }

        private Color _borderColor;
        public Color BorderColor
        {
            get => _borderColor;
            set => SetProperty(ref _borderColor, value);
        }

        private bool _showData;
        public bool ShowData
        {
            get => _showData;
            set => SetProperty(ref _showData, value);
        }

        #endregion

        #region Constructor

        public GpsViewModel()
        {
            LoadItemsCommand = new Command(async () => await LoadMoreData());
        }

        public async Task LoadMoreData()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            LoadingWait = true;
            BorderColor = Color.FromHex("#a09fed");

            await Task.Delay(100);

            var httpCarNames = await HttpClientHelper.SendAndReadAsync(HttpMethod.Post,
                $"{ApiUrlHelper.Url}/api/GpsCheck", isAuthorization: true, headers: new Dictionary<string, string>()
                {
                    {"ChassisNumber", (await App.FullInfoService.GetAsync()).CarShassisNumber}
                });

            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                var desc = JsonConvert.DeserializeObject<GpsCheckModel>(httpCarNames.ResponseMessage);

                BorderColor = desc.IsActiveGPS ? Color.Green : Color.Red;

                GpsState = $"وضعیت: {(desc.IsActiveGPS ? "فعال" : "غیر فعال")}";
                GpsMessage = desc.Message;
                GpsStateImage = desc.IsActiveGPS ? "check_black" : "removeblack";
            }
            else
            {
                GpsState = "وضعیت: نامشخص";
                GpsMessage = "خطایی در درخواست ارسال شده رخ داد.";
                GpsStateImage = "removeblack";
            }

            ShowData = true;

            LoadingWait = false;
            IsBusy = true;
        }

        #endregion

    }
}
