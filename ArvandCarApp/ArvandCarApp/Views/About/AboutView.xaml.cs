﻿using ArvandCarApp.DependencyInterfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.About
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutView
    {
        public AboutView()
        {
            InitializeComponent();


            #region BackPageButton

            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) => await Navigation.PopAsync();
            BackbtnImage.GestureRecognizers.Add(back);

            #endregion

            var data = DependencyService.Get<IBuildDataService>();
            FooterLabel.Text = $"نسخه: {data.VersionName}";

        }
    }
}