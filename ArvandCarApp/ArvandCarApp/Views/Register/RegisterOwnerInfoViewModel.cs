﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ArvandCarApp.Custom;
using ArvandCarApp.DependencyInterfaces;
using Xamarin.Forms;

namespace ArvandCarApp.Views.Register
{
    public class RegisterOwnerInfoViewViewModel
    {
        public ObservableCollection<int> SelectedDate { get; set; }
        public ObservableCollection<object> ItemsSource { get; set; }
        public ObservableCollection<object> Selecteditem { get; set; }

        public RegisterOwnerInfoViewViewModel()
        {
            var dateService = DependencyService.Get<IPersianDateService>();
            var dateNow = dateService.GetPersianDateArray();

            var nguid = new CustomDatePicker();
            ItemsSource = nguid.Date;
            SelectedDate = new ObservableCollection<int>()
            {
                int.Parse(dateNow[0].Substring(dateNow[0].Length - 2, 2)),
                int.Parse(dateNow[1]) - 1,
                int.Parse(dateNow[2]) - 1
            };
            var y = ((IList<object>) ItemsSource[0]).ToArray();
            var m = ((IList<object>) ItemsSource[1]).ToArray();
            var d = ((IList<object>) ItemsSource[2]).ToArray();
            Selecteditem = new ObservableCollection<object>()
            {
                y[int.Parse(SelectedDate[0].ToString())],
                m[int.Parse(SelectedDate[1].ToString())],
                d[int.Parse(SelectedDate[2].ToString())],
            };
        }

    }
}