﻿using System.Net;
using System.Net.Http;
using ArvandCarApp.Helpers;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.Law
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LawView
    {
        public LawView()
        {
            InitializeComponent();

            #region BackPageButton

            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) => await Navigation.PopAsync();
            BackPageButton.GestureRecognizers.Add(back);

            #endregion

        }

        protected override async void OnAppearing()
        {
            var httpCarNames = await HttpClientHelper.SendAndReadAsync(HttpMethod.Get, $"{ApiUrlHelper.Url}/api/law/Clearance");
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                var desc = JsonConvert.DeserializeObject<string>(httpCarNames.ResponseMessage);
                LabelLaw.Text = desc;
            }
            else
            {
                LabelLaw.Text = "امکان دریافت قوانین وجود ندارد.";
            }

            Loading.IsVisible = false;
            LabelLaw.IsVisible = true;
        }

    }
}