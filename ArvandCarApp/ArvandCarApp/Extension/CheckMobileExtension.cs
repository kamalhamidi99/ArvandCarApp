﻿namespace ArvandCarApp.Extension
{
    internal static class CheckMobileExtension
    {
        internal static bool CheckMobile(this string number)
        {
            if (number.Trim().Length == 11)
            {
                if (number.Substring(0, 2) == "09")
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
            return false;
        }

    }
}
