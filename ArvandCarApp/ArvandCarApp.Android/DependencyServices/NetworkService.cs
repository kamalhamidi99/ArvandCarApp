using Android.Net;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Droid.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(NetworkService))]
namespace ArvandCarApp.Droid.DependencyServices
{
    public class NetworkService : INetworkService
    {
        public bool IsOnline()
        {
            var activeConnection = MainActivity.ConnectivityManager.ActiveNetworkInfo;
            return (activeConnection != null) && activeConnection.IsConnected;
        }

        public bool IsWifi()
        {
            var activeConnection = MainActivity.ConnectivityManager.ActiveNetworkInfo;
            return activeConnection.Type == ConnectivityType.Wifi;
        }

        public string GetNetworkType()
        {
            var activeConnection = MainActivity.ConnectivityManager.ActiveNetworkInfo;
            return activeConnection.Type.ToString();
        }
    }
}