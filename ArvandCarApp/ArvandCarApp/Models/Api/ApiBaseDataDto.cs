﻿using System.Collections.Generic;

namespace ArvandCarApp.Models.Api
{
    public class ApiBaseDataDto
    {
        public ApiBaseDataDto()
        {
            Brigades = new List<ApiBaseDataDto>();
        }

        public long Id { get; set; }

        public string Title { get; set; }

        public IEnumerable<ApiBaseDataDto> Brigades { get; set; }
    }
}
