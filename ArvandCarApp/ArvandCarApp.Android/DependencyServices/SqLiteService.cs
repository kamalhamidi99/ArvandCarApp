using System;
using System.IO;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Droid.DependencyServices;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(SqLiteService))]
namespace ArvandCarApp.Droid.DependencyServices
{
    public class SqLiteService : ISqLiteService
    {
        public SQLiteConnection DbConnection()
        {
            var dpPath = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                $"{nameof(ArvandCarApp)}.db3");
            return new SQLiteConnection(dpPath);
        }

    }
}