﻿using Android.App;
using Android.Widget;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Droid.DependencyServices;

[assembly: Xamarin.Forms.Dependency(typeof(MessageToast))]
namespace ArvandCarApp.Droid.DependencyServices
{
    public class MessageToast : IMessageToast
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }
    }
}