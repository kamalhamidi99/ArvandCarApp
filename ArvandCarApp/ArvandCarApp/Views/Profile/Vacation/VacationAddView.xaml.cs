﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using ArvandCarApp.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.Vacation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VacationAddView
    {
        private int _buttonMorakhasiType = 2;
        private int _buttonMonthType = 1;
        private int _buttonMorakhasiDayType;
        private int _buttonMorakhasiPelakType = 0;

        public VacationAddView()
        {
            InitializeComponent();

            #region BackPageButton

            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) => await Navigation.PopAsync();
            BackPageButton.GestureRecognizers.Add(back);

            #endregion
        }

        private async void Button_Send(object sender, EventArgs e)
        {
            Background.IsVisible = true;
            BackgroundLoading.IsVisible = true;

            var headers = new Dictionary<string, string>()
            {
                {"MorakhasiType", _buttonMorakhasiType.ToString()},
                {"MorakhasiDayType", _buttonMorakhasiDayType.ToString()},
                {"MonthType", _buttonMonthType.ToString()},
                {"MorakhasiPelakType", _buttonMorakhasiPelakType.ToString()},
            };

            var url = $"{ApiUrlHelper.Url}/api/vacation/add";

            var sendData = await HttpClientHelper.SendAndReadAsync(HttpMethod.Post, url, headers, isAuthorization: true);
            if (sendData.StatusCode == HttpStatusCode.OK)
            {
                await DisplayAlert("درخواست مرخصی", "مرخصی شما با موفقیت ثبت شد.", "بستن");
                await App.VacationItemService.DeleteAllAsync();
                await Navigation.PopAsync();
                return;
            }

            Background.IsVisible = false;
            BackgroundLoading.IsVisible = false;

            await DisplayAlert("خطا", sendData.ResponseMessage.Trim('"'), "بستن");
        }

        #region Button

        private void ButtonMorakhasiTypeLabel1_OnClicked(object sender, EventArgs e)
        {
            _buttonMorakhasiType = 1;
            ButtonMorakhasiTypeLabel1.BackgroundColor = Color.FromHex("#3d7dad");
            ButtonMorakhasiTypeLabel1.TextColor = Color.White;
            ButtonMorakhasiTypeLabel2.BackgroundColor = Color.White;
            ButtonMorakhasiTypeLabel2.TextColor = Color.Black;
        }

        private void ButtonMorakhasiTypeLabel2_OnClicked(object sender, EventArgs e)
        {
            _buttonMorakhasiType = 2;
            ButtonMorakhasiTypeLabel2.BackgroundColor = Color.FromHex("#3d7dad");
            ButtonMorakhasiTypeLabel2.TextColor = Color.White;
            ButtonMorakhasiTypeLabel1.BackgroundColor = Color.White;
            ButtonMorakhasiTypeLabel1.TextColor = Color.Black;
        }
        
        private void ButtonMonthTypeLabel1_OnClicked(object sender, EventArgs e)
        {
            _buttonMonthType = 0;
            ButtonMonthTypeLabel1.BackgroundColor = Color.FromHex("#3d7dad");
            ButtonMonthTypeLabel1.TextColor = Color.White;
            ButtonMonthTypeLabel0.BackgroundColor = Color.White;
            ButtonMonthTypeLabel0.TextColor = Color.Black;
        }

        private void ButtonMonthTypeLabel0_OnClicked(object sender, EventArgs e)
        {
            _buttonMonthType = 1;
            ButtonMonthTypeLabel0.BackgroundColor = Color.FromHex("#3d7dad");
            ButtonMonthTypeLabel0.TextColor = Color.White;
            ButtonMonthTypeLabel1.BackgroundColor = Color.White;
            ButtonMonthTypeLabel1.TextColor = Color.Black;
        }
        
        private void ButtonMorakhasiDayTypeLabel0_OnClicked(object sender, EventArgs e)
        {
            _buttonMorakhasiDayType = 0;
            ButtonMorakhasiDayTypeLabel0.BackgroundColor = Color.FromHex("#3d7dad");
            ButtonMorakhasiDayTypeLabel0.TextColor = Color.White;
            ButtonMorakhasiDayTypeLabel1.BackgroundColor = Color.White;
            ButtonMorakhasiDayTypeLabel1.TextColor = Color.Black;
            ButtonMorakhasiDayTypeLabel2.BackgroundColor = Color.White;
            ButtonMorakhasiDayTypeLabel2.TextColor = Color.Black;
        }

        private void ButtonMorakhasiDayTypeLabel1_OnClicked(object sender, EventArgs e)
        {
            _buttonMorakhasiDayType = 1;
            ButtonMorakhasiDayTypeLabel1.BackgroundColor = Color.FromHex("#3d7dad");
            ButtonMorakhasiDayTypeLabel1.TextColor = Color.White;
            ButtonMorakhasiDayTypeLabel0.BackgroundColor = Color.White;
            ButtonMorakhasiDayTypeLabel0.TextColor = Color.Black;
            ButtonMorakhasiDayTypeLabel2.BackgroundColor = Color.White;
            ButtonMorakhasiDayTypeLabel2.TextColor = Color.Black;
        }

        private void ButtonMorakhasiDayTypeLabel2_OnClicked(object sender, EventArgs e)
        {
            _buttonMorakhasiDayType = 2;
            ButtonMorakhasiDayTypeLabel2.BackgroundColor = Color.FromHex("#3d7dad");
            ButtonMorakhasiDayTypeLabel2.TextColor = Color.White;
            ButtonMorakhasiDayTypeLabel0.BackgroundColor = Color.White;
            ButtonMorakhasiDayTypeLabel0.TextColor = Color.Black;
            ButtonMorakhasiDayTypeLabel1.BackgroundColor = Color.White;
            ButtonMorakhasiDayTypeLabel1.TextColor = Color.Black;
        }

        #endregion
    }
}