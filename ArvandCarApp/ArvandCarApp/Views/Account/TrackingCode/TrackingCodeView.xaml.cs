﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Helpers;
using ArvandCarApp.Models.Api;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Account.TrackingCode
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrackingCodeView
    {
        private IEnumerable<ApiBaseDataDto> _carNames;

        public TrackingCodeView()
        {
            InitializeComponent();
            BindingContext = new TrackingCodeViewModel();

            var tgr = new TapGestureRecognizer();
            tgr.Tapped += (s, e) => Navigation.PopAsync();
            BackPage.GestureRecognizers.Add(tgr);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var httpCarNames = await HttpClientHelper.SendAndReadAsync(HttpMethod.Get, $"{ApiUrlHelper.Url}/api/Data/CarName");
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                _carNames = JsonConvert.DeserializeObject<IEnumerable<ApiBaseDataDto>>(httpCarNames.ResponseMessage);
            }
        }

        private async void GetTrackingCodeByShasiNumber(object sender, EventArgs e)
        {
            var shasiNumber = ShasiNumberEntry.Text;
            if (string.IsNullOrWhiteSpace(shasiNumber))
            {
                await DisplayAlert("خطا", "شماره شاسی را وارد کنید.", "بستن");
                return;
            }

            if (shasiNumber.Length < 17)
            {
                await DisplayAlert("خطا", "شماره وارد شده معتبر نمی باشد.", "بستن");
                return;
            }

            var url = $"{ApiUrlHelper.Url}/api/TrackingCode/ByShasiNumber";

            var data = await HttpClientHelper.SendAndReadAsync(HttpMethod.Post, url, new Dictionary<string, string>()
            {
                {"ShasiNumber", shasiNumber}
            });

            if (data.StatusCode == HttpStatusCode.OK)
            {
                ShasiNumberEntry.Text = "";
                await DisplayAlert("بازیابی کد رهگیری", $"کد رهگیری درخواست شده {data.ResponseMessage} می باشد.",
                    "بستن");
                return;
            }

            await DisplayAlert("خطا", "امکان دریافت کد رهگیری وجود ندارد.", "بستن");
        }

        private async void GetTrackingCodeByOwnerAndCar(object sender, EventArgs e)
        {
            var ownerMeliCode = OwnerMeliCodeEntry.Text;
            if (string.IsNullOrWhiteSpace(ownerMeliCode))
            {
                await DisplayAlert("خطا", "کد ملی مالک را وارد کنید", "بستن");
                return;
            }

            var datePickerObject = (IEnumerable<int>)DatePicker.SelectedIndex;
            var datePicker = datePickerObject.Select(x => int.Parse(x.ToString())).ToList();
            if (datePicker.Any(x => x < 0))
            {
                await DisplayAlert("خطا", "تاریخ تولد مالک را وارد کنید", "بستن");
                return;
            }

            if (ButtonCarNamePicker.Text.Equals("انتخاب کنید"))
            {
                await DisplayAlert("خطا", "نام خودرو را وارد کنید", "بستن");
                return;
            }

            var carName = _carNames.FirstOrDefault(x => x.Title == ButtonCarNamePicker.Text);
            if (carName == null)
            {
                await DisplayAlert("خطا", "نام خودرو را وارد کنید", "بستن");
                return;
            }

            if (ButtonCarTipPicker.Text.Equals("انتخاب کنید"))
            {
                await DisplayAlert("خطا", "مدل خودرو را وارد کنید", "بستن");
                return;
            }

            var carTip = carName.Brigades.FirstOrDefault(x => x.Title == ButtonCarTipPicker.Text);
            if (carTip == null)
            {
                await DisplayAlert("خطا", "مدل خودرو را وارد کنید", "بستن");
                return;
            }

            var dateService = DependencyService.Get<IPersianDateService>();
            var wheelYear = new ObservableCollection<string>(Enumerable.Range(1300, 1396).Select(year => year.ToString()));
            var ownerBirthdayStr =
                $"{wheelYear[datePicker[0]]}/{dateService.GetPersianMonthName().First(x => x.Key == datePicker[1] + 1).Key}/{datePicker[2] + 1}";

            var data = await HttpClientHelper.SendAndReadAsync(HttpMethod.Post,
                $"{ApiUrlHelper.Url}/api/TrackingCode/ByOwnerAndCar", new Dictionary<string, string>()
                {
                    {"ownerMeliCode", ownerMeliCode},
                    {"ownerBirthdayStr", ownerBirthdayStr},
                    {"fkCarName", carName.Id.ToString()},
                    {"fkCarTip", carTip.Id.ToString()},
                });

            if (data.StatusCode == HttpStatusCode.OK)
            {
                ShasiNumberEntry.Text = "";
                await DisplayAlert("بازیابی کد رهگیری", $"کد رهگیری درخواست شده {data.ResponseMessage} می باشد.",
                    "بستن");
                return;
            }

            await DisplayAlert("خطا", "امکان دریافت کد رهگیری وجود ندارد.", "بستن");
        }

        private void WithShasiNumber(object sender, EventArgs e)
        {
            WithShasiNumberLine.IsVisible = true;
            WithShasiNumberContent.IsVisible = true;
            WithOwnerAndCarLine.IsVisible = false;
            WithOwnerAndCarContent.IsVisible = false;
        }

        private void WithOwnerAndCar(object sender, EventArgs e)
        {
            WithShasiNumberLine.IsVisible = false;
            WithShasiNumberContent.IsVisible = false;
            WithOwnerAndCarLine.IsVisible = true;
            WithOwnerAndCarContent.IsVisible = true;
        }

        private async void ButtonCarNamePicker_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("نام خودرو", "بستن", null, _carNames.Select(x => x.Title).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                ButtonCarNamePicker.Text =  text;
            }
        }

        private async void ButtonCarTipPicker_OnClicked(object sender, EventArgs e)
        {
            if (ButtonCarNamePicker.Text.Equals("انتخاب کنید"))
            {
                await DisplayAlert("خطا", "نام خودرو را انتخاب کنید", "بستن");
                return;
            }

            var carName = _carNames.FirstOrDefault(x => x.Title == ButtonCarNamePicker.Text);
            if (carName == null)
            {
                await DisplayAlert("خطا", "نام خودرو را انتخاب کنید", "بستن");
                return;
            }

            var text = await DisplayActionSheet("مدل خودرو", "بستن", null,
                carName.Brigades.Select(x => x.Title).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                ButtonCarTipPicker.Text = text;
            }
        }
    }
}
