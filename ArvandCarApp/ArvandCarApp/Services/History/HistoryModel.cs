﻿using SQLite;
using Xamarin.Forms;

namespace ArvandCarApp.Services.History
{
    public class HistoryModel
    {
        [PrimaryKey]
        public int Id { get; set; }

        /// <summary>
        /// عنوان
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// پیام
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// تاریخ
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// روز
        /// </summary>
        public string DateDay { get; set; }
        /// <summary>
        /// ماه
        /// </summary>
        public string DateMonth { get; set; }

        [Ignore]
        public Color Color { get; set; }
    }
}
