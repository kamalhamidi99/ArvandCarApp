﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Extension;
using ArvandCarApp.Services.RegisterNewCar;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Register
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterOwnerInfoView
    {
        private readonly RegisterOwnerInfoViewViewModel _viewModel;

        public RegisterOwnerInfoView()
        {
            InitializeComponent();
            _viewModel = new RegisterOwnerInfoViewViewModel();
            //BindingContext = viewModel;

            // back button
            var tgr = new TapGestureRecognizer();
            tgr.Tapped += (s, e) => Navigation.PopAsync();
            BackPage.GestureRecognizers.Add(tgr);

            //var display = DependencyService.Get<IDisplayService>();
            //StackLayoutContent.WidthRequest = display.Width;

            PickerBirthday.ItemsSource = _viewModel.ItemsSource;
            OwnerType.Text = "حقیقی";
            ViewOfficGoverment.IsVisible = false;

            // load last data from storge
            LoadData();
        }

        private async void GoToCarView_OnClicked(object sender, EventArgs e)
        {
            var isOk = true;

            var datePicker = (IList<int>)PickerBirthday.SelectedIndex;
            var y = datePicker[0];
            var m = datePicker[1];
            var d = datePicker[2];
            var dateService = DependencyService.Get<IPersianDateService>();
            var wheelYear = new ObservableCollection<string>(Enumerable.Range(1300, 1400).Select(year => year.ToString()));
            var ownerBirthdayStr = $"{wheelYear[datePicker[0]]}/{dateService.GetPersianMonthName().First(x => x.Key == datePicker[1] + 1).Key}/{datePicker[2] + 1}";

            if (string.IsNullOrWhiteSpace(ownerBirthdayStr))
            {
                isOk = false;
            }

            if (string.IsNullOrWhiteSpace(EntryCode.Text))
            {
                isOk = false;
            }
            else if (OwnerType.Text.Equals("حقیقی"))
            {
                if (EntryCode.Text.CheckNationalCode() == false)
                {
                    isOk = false;
                }
            }
            var ownerMeliCode = EntryCode.Text;

            if (string.IsNullOrWhiteSpace(EntryName.Text))
            {
                isOk = false;
            }
            var ownerName = EntryName.Text;

            if (string.IsNullOrWhiteSpace(EntryFather.Text))
            {
                isOk = false;
            }
            var ownerFather = EntryFather.Text;

            if (string.IsNullOrWhiteSpace(EntryShenasname.Text))
            {
                isOk = false;
            }
            var ownerShenasname = EntryShenasname.Text;

            if (string.IsNullOrWhiteSpace(EntrySadere.Text))
            {
                isOk = false;
            }
            var ownerSadere = EntrySadere.Text;

            if (OwnerType.Text.Equals("حقیقی"))
            {
                if (PickerSexType.Text.Equals("انتخاب کنید"))
                {
                    isOk = false;
                }
            }
            var ownerSex = PickerSexType.Text;

            if (string.IsNullOrWhiteSpace(EntryPhone.Text))
            {
                isOk = false;
            }
            var ownerTell = EntryPhone.Text;

            if (string.IsNullOrWhiteSpace(EntryMobile.Text))
            {
                isOk = false;
            }
            else if (EntryMobile.Text.CheckMobile() == false)
            {
                isOk = false;
            }
            var ownerMobile = EntryMobile.Text;

            if (string.IsNullOrWhiteSpace(EntryAddress.Text))
            {
                isOk = false;
            }
            var ownerAddress = EntryAddress.Text;

            if (isOk == false)
            {
                await DisplayAlert("خطا", "لطفا اطلاعات ضروری را وارد کنید", "بستن");
                return;
            }

            var ishaghighi = OwnerType.Text.Equals("حقیقی");
            var isofficGoverMent = PickerOfficGoverment.Text.Equals("اداره دولتی است");
            var isownerSex = ownerSex.Equals("مرد") ? "1" : "2";

            var registerNewCarService = new RegisterNewCarService();
            var data = registerNewCarService.Get();
            data.Ishaghighi = OwnerType.Text;
            data.IsofficGoverMent = PickerOfficGoverment.Text;
            data.OwnerMeliCode = EntryCode.Text;
            data.OwnerName = EntryName.Text;
            data.OwnerFather = EntryFather.Text;
            data.OwnerShenasname = EntryShenasname.Text;
            data.OwnerSadere = EntrySadere.Text;
            data.OwnerBirthdayStrYear = y.ToString();
            data.OwnerBirthdayStrMonth = m.ToString();
            data.OwnerBirthdayStrDay = d.ToString();
            data.OwnerSex = ownerSex;
            data.OwnerTell = EntryPhone.Text;
            data.OwnerMobile = EntryMobile.Text;
            data.OwnerAddress = EntryAddress.Text;
            registerNewCarService.StoreData(data);

            await Navigation.PushAsync(new RegisterCarInfoView(ishaghighi: ishaghighi, isofficGoverMent: isofficGoverMent,
                ownerMeliCode: ownerMeliCode, ownerName: ownerName, ownerFather: ownerFather,
                ownerShenasname: ownerShenasname, ownerSadere: ownerSadere, ownerBirthdayStr: ownerBirthdayStr,
                ownerSex: byte.Parse(isownerSex), ownerTell: ownerTell, ownerMobile: ownerMobile,
                ownerAddress: ownerAddress));
        }

        private void LoadData()
        {
            var data = new RegisterNewCarService().Get();
            if (data.Id > 0)
            {
                OwnerType.Text = data.Ishaghighi;
                EntryCode.Text = data.OwnerMeliCode;
                EntryName.Text = data.OwnerName;
                EntryFather.Text = data.OwnerFather;
                EntryShenasname.Text = data.OwnerShenasname;
                EntrySadere.Text = data.OwnerSadere;
                PickerOfficGoverment.Text = data.IsofficGoverMent;
                PickerSexType.Text = data.OwnerSex;
                EntryPhone.Text = data.OwnerTell;
                EntryMobile.Text = data.OwnerMobile;
                EntryAddress.Text = data.OwnerAddress;
                PickerBirthday.SelectedIndex = new ObservableCollection<int>()
                {
                    int.Parse(data.OwnerBirthdayStrYear),
                    int.Parse(data.OwnerBirthdayStrMonth),
                    int.Parse(data.OwnerBirthdayStrDay),
                };
            }
            else
            {
                PickerBirthday.SelectedIndex = _viewModel.SelectedDate;
            }

            FixOwnerType();
        }

        private async void OwnerType_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("نوع مالک", "بستن", null, "حقیقی", "حقوقی");
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                OwnerType.Text = text;
                FixOwnerType();
            }
        }

        private void FixOwnerType()
        {
            if (OwnerType.Text.Equals("حقیقی"))
            {
                LabelCode.Text = "کد ملی";
                LabelName.Text = "نام و نام خانوادگی";
                LabelFather.Text = "نام پدر";
                LabelShenasname.Text = "شماره شناسنامه";
                LabelSadere.Text = "محل صدور";
                LabelBirthday.Text = "تاریخ تولد";
                ViewSexType.IsVisible = true;
                ViewOfficGoverment.IsVisible = false;
            }
            else
            {
                LabelCode.Text = "شناسه ملی";
                LabelName.Text = "نام شرکت";
                LabelFather.Text = "نام مدیر";
                LabelShenasname.Text = "شماره ثبت";
                LabelSadere.Text = "محل ثبت";
                LabelBirthday.Text = "تاریخ ثبت";
                ViewSexType.IsVisible = false;
                ViewOfficGoverment.IsVisible = true;
            }
        }

        private async void PickerSexType_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("جنسیت", "بستن", null, "مرد", "زن");
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerSexType.Text = text;
            }
        }

        private async void PickerOfficGoverment_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("اداره دولتی", "بستن", null, "اداره دولتی است", "اداره دولتی نیست");
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerOfficGoverment.Text = text;
            }
        }

    }
}
