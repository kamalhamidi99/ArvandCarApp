﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Expire
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExpireView : ContentPage
    {
        public ExpireView()
        {
            InitializeComponent();
        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://cafebazaar.ir/app/arvandfreezone.car/?l=fa"));
        }
    }
}