﻿using System;
using System.Text;
using ArvandCarApp.Services.Login;
using ArvandCarApp.Views.Account.MobileNumber;
using ArvandCarApp.Views.Account.TrackingCode;
using ArvandCarApp.Views.Register;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Account.Login
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginView
    {
        public LoginView()
        {
            InitializeComponent();

            var tgr = new TapGestureRecognizer();
            tgr.Tapped += (s, e) => Navigation.PushAsync(new TrackingCodeView());
            GetTrackingCodeLabel.GestureRecognizers.Add(tgr);

            UserNameEntry.Text = ""; //1741187311
            PasswordEntry.Text = ""; //132049

            UserNameEntry.Completed += (s, e) => PasswordEntry.Focus();
        }

        private async void LoginClick(object sender, EventArgs e)
        {
            //var r = await new TestService().Add();
            //await DisplayAlert("-", r.ToString(), "ok");

            if (string.IsNullOrWhiteSpace(UserNameEntry.Text) || string.IsNullOrWhiteSpace(PasswordEntry.Text))
            {
                await DisplayAlert("خطا", "کد ملی یا کد رهگیری وارد نشده است.", "بستن");
                return;
            }

            BackgroundLoading.IsVisible = true;
            Loading.IsVisible = true;

            var result = await App.LoginService.SendAsync(UserNameEntry.Text, PasswordEntry.Text);
            if (string.IsNullOrWhiteSpace(result))
            {
                var isInsert = await App.LoginService.AddAsync(new LoginModel()
                {
                    Username = UserNameEntry.Text,
                    Password = PasswordEntry.Text,
                });

                if (!isInsert)
                    await DisplayAlert("خطا", "امکان ذخیره اطلاعات ورود وجود ندارد", "بستن");

                //Analytics.TrackEvent($"Loggin by {PasswordEntry.Text}");

                // دریافت اطلاعات
                //await App.FullInfoService.LoadAsync();

                Navigation.InsertPageBefore(new MobileNumberView(), Navigation.NavigationStack[0]);
                await Navigation.PopToRootAsync();
            }
            else
            {
                await DisplayAlert("خطا", result.Trim('"'), "بستن");
            }

            BackgroundLoading.IsVisible = false;
            Loading.IsVisible = false;
        }

        private async void RegisterNewCar(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AcceptLawView());
        }

        private async void HelpLogin_OnClicked(object sender, EventArgs e)
        {
            var str = new StringBuilder();
            str.AppendLine("کاربر گرامی برای ورود، ابتدا کد ملی متقاضی و کد رهگیری خودرو را وارد کنید.");
            str.AppendLine("- چنانچه خودرو مورد نظر دارای نقل و انتقال می باشد، کد ملی صاحب جدید خودرو را وارد کنید.");
            str.AppendLine("- کد رهگیری خودرو، در برگه 'پروانه تردد یکساله خودرو' با عنوان 'شناسه' درج شده است.");
            await DisplayAlert("راهنما", str.ToString(), "بستن");
        }
    }
}
