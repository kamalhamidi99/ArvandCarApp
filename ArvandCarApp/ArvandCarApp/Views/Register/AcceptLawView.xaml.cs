﻿using System;
using System.Net;
using System.Net.Http;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Helpers;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Register
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AcceptLawView
    {
        public AcceptLawView()
        {
            InitializeComponent();

            //back button
            var tgr = new TapGestureRecognizer();
            tgr.Tapped += BackButtonToolbar;
            BackPage.GestureRecognizers.Add(tgr);

            var display = DependencyService.Get<IDisplayService>();
            StackLayoutContent.WidthRequest = display.Width;


            FabButton.Clicked = async (sender, args) =>
            {
                await Navigation.PushAsync(new RegisterOwnerInfoView());
            };
        }

        protected override async void OnAppearing()
        {
            var httpCarNames = await HttpClientHelper.SendAndReadAsync(HttpMethod.Get, $"{ApiUrlHelper.Url}/api/law/Clearance");
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                var desc = JsonConvert.DeserializeObject<string>(httpCarNames.ResponseMessage);
                LabelLaw.Text = desc;
            }
            else
            {
                LabelLaw.Text = "امکان دریافت قوانین وجود ندارد.";
            }

            LabelWait.IsVisible = false;
            Loading.IsVisible = false;

            FabButton.IsVisible = true;
            LabelLaw.IsVisible = true;
        }

        private async void BackButtonToolbar(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

    }
}
