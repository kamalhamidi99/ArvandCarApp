﻿using System.Linq;
using System.Threading.Tasks;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Services.Login;
using SQLite;
using Xamarin.Forms;
using XLabs.Platform.Services.IO;

namespace ArvandCarApp
{
    public class TestService
    {
        private readonly SQLiteAsyncConnection _db;

        public TestService()
        {
            _db = new SQLiteAsyncConnection(DependencyService.Get<IFileService>().GetLocalFilePath("test.db3"));
        }

       
        public async Task<int> Add()
        {
            await _db.CreateTableAsync<TestClass>();

            var model = new TestClass()
            {
                Name = "kamal",
            };

            await _db.InsertAsync(model);

            return await _db.Table<TestClass>().CountAsync();
        }
    }

    public class TestClass
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }
    }

}