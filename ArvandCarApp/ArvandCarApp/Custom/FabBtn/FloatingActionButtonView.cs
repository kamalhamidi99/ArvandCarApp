using System;
using Xamarin.Forms;

namespace ArvandCarApp.Custom.FabBtn
{
    public class FloatingActionButtonView : View
    {
        public static readonly BindableProperty ImageNameProperty =
            BindableProperty.Create(propertyName: nameof(String), returnType: typeof(string), declaringType: typeof(string), defaultValue: string.Empty);
        public string ImageName
        {
            get => (string)GetValue(ImageNameProperty);
            set => SetValue(ImageNameProperty, value);
        }

        public static readonly BindableProperty ColorNormalProperty =
            BindableProperty.Create(propertyName: "ColorNorsmal", returnType: typeof(Color), declaringType: typeof(Color), defaultValue: Color.White);
        public Color ColorNormal
        {
            get => (Color)GetValue(ColorNormalProperty);
            set => SetValue(ColorNormalProperty, value);
        }

        public static readonly BindableProperty ColorPressedProperty =
            BindableProperty.Create(propertyName: "ColorPressexd", returnType: typeof(Color), declaringType: typeof(Color), defaultValue: Color.White);
        public Color ColorPressed
        {
            get => (Color)GetValue(ColorPressedProperty);
            set => SetValue(ColorPressedProperty, value);
        }

        public static readonly BindableProperty ColorRippleProperty =
            BindableProperty.Create(propertyName: "ColorRipple", returnType: typeof(Color), declaringType: typeof(Color), defaultValue: Color.White);
        public Color ColorRipple
        {
            get => (Color)GetValue(ColorRippleProperty);
            set => SetValue(ColorRippleProperty, value);
        }

        public static readonly BindableProperty SizeProperty =
            BindableProperty.Create(propertyName: "Size", returnType: typeof(FloatingActionButtonSize), declaringType: typeof(FloatingActionButtonSize), defaultValue: FloatingActionButtonSize.Normal);
        public FloatingActionButtonSize Size
        {
            get => (FloatingActionButtonSize)GetValue(SizeProperty);
            set => SetValue(SizeProperty, value);
        }

        public static readonly BindableProperty HasShadowProperty =
            BindableProperty.Create(propertyName: "HasShadow", returnType: typeof(bool), declaringType: typeof(bool), defaultValue: true);
        public bool HasShadow
        {
            get => (bool)GetValue(HasShadowProperty);
            set => SetValue(HasShadowProperty, value);
        }

        public delegate void ShowHideDelegate(bool animate = true);
        public delegate void AttachToListViewDelegate(ListView listView);

        public ShowHideDelegate Show { get; set; }
        public ShowHideDelegate Hide { get; set; }
        public Action<object, EventArgs> Clicked { get; set; }
    }

}

