﻿using System;
using SQLite;

namespace ArvandCarApp.Services.NumberValidation
{
    public class NumberValidationModel
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string MobileNumber { get; set; }

        public string ValidationCode { get; set; }

        public bool IsValidate { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
