﻿using Android.App;
using Android.Content.PM;
using Android.OS;

namespace ArvandCarApp.Droid
{
    [Activity(Label = "Arvand Car", Icon = "@drawable/icon", Theme = "@style/Start", MainLauncher = true, ConfigurationChanges = ConfigChanges.Orientation, NoHistory = true)]
    public class StartActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            System.Threading.Thread.Sleep(500);
            StartActivity(typeof(MainActivity));
        }
    }
}