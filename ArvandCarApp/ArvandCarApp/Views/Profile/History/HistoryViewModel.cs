﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ArvandCarApp.Extension;
using ArvandCarApp.Services.History;
using ArvandCarApp.ViewModels;
using Xamarin.Forms;

namespace ArvandCarApp.Views.Profile.History
{
    public class HistoryViewModel : BaseViewModel
    {
        #region Properties

        public Command LoadItemsCommand { get; set; }

        private ObservableCollection<HistoryModel> _historyModels;
        public ObservableCollection<HistoryModel> HistoryModels
        {
            get => _historyModels;
            set => SetProperty(ref _historyModels, value);
        }

        private bool _loadingWait;
        public bool LoadingWait
        {
            get => _loadingWait;
            set => SetProperty(ref _loadingWait, value);
        }

        #endregion

        #region Constructor

        public HistoryViewModel()
        {
            HistoryModels = new ObservableCollection<HistoryModel>();
            LoadItemsCommand = new Command(async () => await LoadMoreData());
        }

        public async Task LoadMoreData()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            LoadingWait = true;

            await Task.Delay(100);

            IEnumerable<HistoryModel> allHistoryModels = await App.HistoryService.LoadAsync();

            if (allHistoryModels.Any())
            {
                HistoryModels.Clear();

                foreach (var model in allHistoryModels)
                {
                    model.Color = RandomColorExtension.Get();
                    HistoryModels.Add(model);
                }
            }

            LoadingWait = false;
            IsBusy = true;
        }

        #endregion

    }
}
