﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using ArvandCarApp.Services.Vacation.VacationItem;
using Xamarin.Forms;

namespace ArvandCarApp.Views.Profile.Vacation
{
    public class VacationMainViewModel : INotifyPropertyChanged
    {
        #region Fields

        private bool _isBusy;
        private IList<VacationItemModel> _allVacationItemModels;

        #endregion

        #region Properties

        private ObservableCollection<VacationItemModel> _vacationItemModels;
        public ObservableCollection<VacationItemModel> VacationItemModels
        {
            get => _vacationItemModels;
            set
            {
                _vacationItemModels = value;
                OnPropertyChanged("VacationItemModels");
            }
        }

        private bool _loadingWait;
        public bool LoadingWait
        {
            get => _loadingWait;
            set
            {
                _loadingWait = value;
                OnPropertyChanged("LoadingWait");
            }
        }

        #region MorakhasiType

        private Color _buttonMorakhasiTypeLabel1BackgroundColor;
        public Color ButtonMorakhasiTypeLabel1BackgroundColor
        {
            get => _buttonMorakhasiTypeLabel1BackgroundColor;
            set
            {
                _buttonMorakhasiTypeLabel1BackgroundColor = value;
                OnPropertyChanged("ButtonMorakhasiTypeLabel1BackgroundColor");
            }
        }

        private Color _buttonMorakhasiTypeLabel1TextColor;
        public Color ButtonMorakhasiTypeLabel1TextColor
        {
            get => _buttonMorakhasiTypeLabel1TextColor;
            set
            {
                _buttonMorakhasiTypeLabel1TextColor = value;
                OnPropertyChanged("ButtonMorakhasiTypeLabel1TextColor");
            }
        }

        private Color _buttonMorakhasiTypeLabel2BackgroundColor;
        public Color ButtonMorakhasiTypeLabel2BackgroundColor
        {
            get => _buttonMorakhasiTypeLabel2BackgroundColor;
            set
            {
                _buttonMorakhasiTypeLabel2BackgroundColor = value;
                OnPropertyChanged("ButtonMorakhasiTypeLabel2BackgroundColor");
            }
        }

        private Color _buttonMorakhasiTypeLabel2TextColor;
        public Color ButtonMorakhasiTypeLabel2TextColor
        {
            get => _buttonMorakhasiTypeLabel2TextColor;
            set
            {
                _buttonMorakhasiTypeLabel2TextColor = value;
                OnPropertyChanged("ButtonMorakhasiTypeLabel2TextColor");
            }
        }

        #endregion

        #region MonthType

        private Color _buttonMonthTypeLabel0BackgroundColor;
        public Color ButtonMonthTypeLabel0BackgroundColor
        {
            get => _buttonMonthTypeLabel0BackgroundColor;
            set
            {
                _buttonMonthTypeLabel0BackgroundColor = value;
                OnPropertyChanged("ButtonMonthTypeLabel0BackgroundColor");
            }
        }

        private Color _buttonMonthTypeLabel0TextColor;
        public Color ButtonMonthTypeLabel0TextColor
        {
            get => _buttonMonthTypeLabel0TextColor;
            set
            {
                _buttonMonthTypeLabel0TextColor = value;
                OnPropertyChanged("ButtonMonthTypeLabel0TextColor");
            }
        }

        private Color _buttonMonthTypeLabel1BackgroundColor;
        public Color ButtonMonthTypeLabel1BackgroundColor
        {
            get => _buttonMonthTypeLabel1BackgroundColor;
            set
            {
                _buttonMonthTypeLabel1BackgroundColor = value;
                OnPropertyChanged("ButtonMonthTypeLabel1BackgroundColor");
            }
        }

        private Color _buttonMonthTypeLabel1TextColor;
        public Color ButtonMonthTypeLabel1TextColor
        {
            get => _buttonMonthTypeLabel1TextColor;
            set
            {
                _buttonMonthTypeLabel1TextColor = value;
                OnPropertyChanged("ButtonMonthTypeLabel1TextColor");
            }
        }

        #endregion

        #region MorakhasiPelakType

        private Color _buttonMorakhasiPelakTypeLabel0BackgroundColor;
        public Color ButtonMorakhasiPelakTypeLabel0BackgroundColor
        {
            get => _buttonMorakhasiPelakTypeLabel0BackgroundColor;
            set
            {
                _buttonMorakhasiPelakTypeLabel0BackgroundColor = value;
                OnPropertyChanged("ButtonMorakhasiPelakTypeLabel0BackgroundColor");
            }
        }

        private Color _buttonMorakhasiPelakTypeLabel0TextColor;
        public Color ButtonMorakhasiPelakTypeLabel0TextColor
        {
            get => _buttonMorakhasiPelakTypeLabel0TextColor;
            set
            {
                _buttonMorakhasiPelakTypeLabel0TextColor = value;
                OnPropertyChanged("ButtonMorakhasiPelakTypeLabel0TextColor");
            }
        }

        private Color _buttonMorakhasiPelakTypeLabel1BackgroundColor;
        public Color ButtonMorakhasiPelakTypeLabel1BackgroundColor
        {
            get => _buttonMorakhasiPelakTypeLabel1BackgroundColor;
            set
            {
                _buttonMorakhasiPelakTypeLabel1BackgroundColor = value;
                OnPropertyChanged("ButtonMorakhasiPelakTypeLabel1BackgroundColor");
            }
        }

        private Color _buttonMorakhasiPelakTypeLabel1TextColor;
        public Color ButtonMorakhasiPelakTypeLabel1TextColor
        {
            get => _buttonMorakhasiPelakTypeLabel1TextColor;
            set
            {
                _buttonMorakhasiPelakTypeLabel1TextColor = value;
                OnPropertyChanged("ButtonMorakhasiPelakTypeLabel1TextColor");
            }
        }

        #endregion

        #region MorakhasiDayType

        private Color _buttonMorakhasiDayTypeLabel1BackgroundColor;
        public Color ButtonMorakhasiDayTypeLabel1BackgroundColor
        {
            get => _buttonMorakhasiDayTypeLabel1BackgroundColor;
            set
            {
                _buttonMorakhasiDayTypeLabel1BackgroundColor = value;
                OnPropertyChanged("ButtonMorakhasiDayTypeLabel1BackgroundColor");
            }
        }

        private Color _buttonMorakhasiDayTypeLabel1TextColor;
        public Color ButtonMorakhasiDayTypeLabel1TextColor
        {
            get => _buttonMorakhasiDayTypeLabel1TextColor;
            set
            {
                _buttonMorakhasiDayTypeLabel1TextColor = value;
                OnPropertyChanged("ButtonMorakhasiDayTypeLabel1TextColor");
            }
        }

        private Color _buttonMorakhasiDayTypeLabel2BackgroundColor;
        public Color ButtonMorakhasiDayTypeLabel2BackgroundColor
        {
            get => _buttonMorakhasiDayTypeLabel2BackgroundColor;
            set
            {
                _buttonMorakhasiDayTypeLabel2BackgroundColor = value;
                OnPropertyChanged("ButtonMorakhasiDayTypeLabel2BackgroundColor");
            }
        }

        private Color _buttonMorakhasiDayTypeLabel2TextColor;
        public Color ButtonMorakhasiDayTypeLabel2TextColor
        {
            get => _buttonMorakhasiDayTypeLabel2TextColor;
            set
            {
                _buttonMorakhasiDayTypeLabel2TextColor = value;
                OnPropertyChanged("ButtonMorakhasiDayTypeLabel2TextColor");
            }
        }

        private Color _buttonMorakhasiDayTypeLabel0BackgroundColor;
        public Color ButtonMorakhasiDayTypeLabel0BackgroundColor
        {
            get => _buttonMorakhasiDayTypeLabel0BackgroundColor;
            set
            {
                _buttonMorakhasiDayTypeLabel0BackgroundColor = value;
                OnPropertyChanged("ButtonMorakhasiDayTypeLabel0BackgroundColor");
            }
        }

        private Color _buttonMorakhasiDayTypeLabel0TextColor;
        public Color ButtonMorakhasiDayTypeLabel0TextColor
        {
            get => _buttonMorakhasiDayTypeLabel0TextColor;
            set
            {
                _buttonMorakhasiDayTypeLabel0TextColor = value;
                OnPropertyChanged("ButtonMorakhasiDayTypeLabel0TextColor");
            }
        }

        #endregion

        public int ButtonMorakhasiType = 2; // 1 تعمیراتی xx 2 استحقاقی
        public int ButtonMonthType = 1; // 1 ماه جاری xx 0 ماه اینده
        public int ButtonMorakhasiDayType; // 0 کل ماه xx 1 نیمه اول xx 2 نیمه دوم
        public int ButtonMorakhasiPelakType; // 0 خرمشهر xx 1 اهواز

        #endregion

        #region Constructor

        public VacationMainViewModel()
        {
            _vacationItemModels = new ObservableCollection<VacationItemModel>();

            _buttonMorakhasiTypeLabel2BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMorakhasiTypeLabel2TextColor = Color.White;
            _buttonMorakhasiTypeLabel1BackgroundColor = Color.White;
            _buttonMorakhasiTypeLabel1TextColor = Color.Black;

            _buttonMonthTypeLabel1BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMonthTypeLabel1TextColor = Color.White;
            _buttonMonthTypeLabel0BackgroundColor = Color.White;
            _buttonMonthTypeLabel0TextColor = Color.Black;

            _buttonMorakhasiPelakTypeLabel1BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMorakhasiPelakTypeLabel1TextColor = Color.White;
            _buttonMorakhasiPelakTypeLabel0BackgroundColor = Color.White;
            _buttonMorakhasiPelakTypeLabel0TextColor = Color.Black;

            _buttonMorakhasiDayTypeLabel0BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMorakhasiDayTypeLabel0TextColor = Color.White;
            _buttonMorakhasiDayTypeLabel1BackgroundColor = Color.White;
            _buttonMorakhasiDayTypeLabel1TextColor = Color.Black;
            _buttonMorakhasiDayTypeLabel2BackgroundColor = Color.White;
            _buttonMorakhasiDayTypeLabel2TextColor = Color.Black;

            LoadMoreData();
        }

        public async Task LoadMoreData()
        {
            _isBusy = false;
            _loadingWait = true;

            await Task.Delay(300);

            if (_isBusy)
                return;

            var data = await App.VacationItemService.LoadAsync();
            if (data.Any())
            {
                _allVacationItemModels = data;
            }
            else
            {
                _allVacationItemModels = await App.VacationItemService.LoadAsync();
            }

            if (_allVacationItemModels == null || !_allVacationItemModels.Any())
            {
                _loadingWait = false;
                OnPropertyChanged("LoadingWait");
                return;
            }

            _allVacationItemModels = FixData(_allVacationItemModels);

            foreach (var model in _allVacationItemModels)
            {
                _vacationItemModels.Add(model);
            }

            _loadingWait = false;
            OnPropertyChanged("LoadingWait");

            _isBusy = true;
        }

        #endregion

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        #region FixData

        private static IList<VacationItemModel> FixData(IList<VacationItemModel> data)
        {
            foreach (var item in data)
            {
                if (item.IsOk == "1" && item.IsPay == "true") // سبز
                {
                    item.Color = "DarkCyan";
                }
                else if (item.IsOk == "2" || item.IsPay == "false") // قرمز
                {
                    item.Color = "DarkRed";
                }
                else
                {
                    item.Color = "CornflowerBlue"; // در حال بررسی
                }

                item.ShowPaying = item.IsPay != "true";
                item.ShowPaied = item.IsPay == "true";
            }

            return data;
        }

        #endregion

        #region Button Command

        public Command ButtonMorakhasiTypeLabel1 => new Command(ButtonMorakhasiTypeLabel1_OnClicked);
        public Command ButtonMorakhasiTypeLabel2 => new Command(ButtonMorakhasiTypeLabel2_OnClicked);

        public Command ButtonMonthTypeLabel1 => new Command(ButtonMonthTypeLabel1_OnClicked);
        public Command ButtonMonthTypeLabel0 => new Command(ButtonMonthTypeLabel0_OnClicked);

        public Command ButtonMorakhasiPelakTypeLabel1 => new Command(ButtonMorakhasiPelakTypeLabel1_OnClicked);
        public Command ButtonMorakhasiPelakTypeLabel0 => new Command(ButtonMorakhasiPelakTypeLabel0_OnClicked);

        public Command ButtonMorakhasiDayTypeLabel0 => new Command(ButtonMorakhasiDayTypeLabel0_OnClicked);
        public Command ButtonMorakhasiDayTypeLabel1 => new Command(ButtonMorakhasiDayTypeLabel1_OnClicked);
        public Command ButtonMorakhasiDayTypeLabel2 => new Command(ButtonMorakhasiDayTypeLabel2_OnClicked);

        #endregion

        #region Button Methods

        private void ButtonMorakhasiTypeLabel1_OnClicked()
        {
            ButtonMorakhasiType = 1;
            _buttonMorakhasiTypeLabel1BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMorakhasiTypeLabel1TextColor = Color.White;
            _buttonMorakhasiTypeLabel2BackgroundColor = Color.White;
            _buttonMorakhasiTypeLabel2TextColor = Color.Black;

            OnPropertyChanged("ButtonMorakhasiTypeLabel1BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiTypeLabel1TextColor");
            OnPropertyChanged("ButtonMorakhasiTypeLabel2BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiTypeLabel2TextColor");
        }

        private void ButtonMorakhasiTypeLabel2_OnClicked()
        {
            ButtonMorakhasiType = 2;
            _buttonMorakhasiTypeLabel2BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMorakhasiTypeLabel2TextColor = Color.White;
            _buttonMorakhasiTypeLabel1BackgroundColor = Color.White;
            _buttonMorakhasiTypeLabel1TextColor = Color.Black;

            OnPropertyChanged("ButtonMorakhasiTypeLabel2BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiTypeLabel2TextColor");
            OnPropertyChanged("ButtonMorakhasiTypeLabel1BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiTypeLabel1TextColor");
        }

        private void ButtonMonthTypeLabel1_OnClicked()
        {
            ButtonMonthType = 1;
            _buttonMonthTypeLabel1BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMonthTypeLabel1TextColor = Color.White;
            _buttonMonthTypeLabel0BackgroundColor = Color.White;
            _buttonMonthTypeLabel0TextColor = Color.Black;

            OnPropertyChanged("ButtonMonthTypeLabel1BackgroundColor");
            OnPropertyChanged("ButtonMonthTypeLabel1TextColor");
            OnPropertyChanged("ButtonMonthTypeLabel0BackgroundColor");
            OnPropertyChanged("ButtonMonthTypeLabel0TextColor");
        }

        private void ButtonMonthTypeLabel0_OnClicked()
        {
            ButtonMonthType = 0;
            _buttonMonthTypeLabel0BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMonthTypeLabel0TextColor = Color.White;
            _buttonMonthTypeLabel1BackgroundColor = Color.White;
            _buttonMonthTypeLabel1TextColor = Color.Black;

            OnPropertyChanged("ButtonMonthTypeLabel0BackgroundColor");
            OnPropertyChanged("ButtonMonthTypeLabel0TextColor");
            OnPropertyChanged("ButtonMonthTypeLabel1BackgroundColor");
            OnPropertyChanged("ButtonMonthTypeLabel1TextColor");
        }

        private void ButtonMorakhasiPelakTypeLabel1_OnClicked()
        {
            ButtonMorakhasiPelakType = 0;
            _buttonMorakhasiPelakTypeLabel1BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMorakhasiPelakTypeLabel1TextColor = Color.White;
            _buttonMorakhasiPelakTypeLabel0BackgroundColor = Color.White;
            _buttonMorakhasiPelakTypeLabel0TextColor = Color.Black;

            OnPropertyChanged("ButtonMorakhasiPelakTypeLabel1BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiPelakTypeLabel1TextColor");
            OnPropertyChanged("ButtonMorakhasiPelakTypeLabel0BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiPelakTypeLabel0TextColor");
        }

        private void ButtonMorakhasiPelakTypeLabel0_OnClicked()
        {
            ButtonMorakhasiPelakType = 1;
            _buttonMorakhasiPelakTypeLabel0BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMorakhasiPelakTypeLabel0TextColor = Color.White;
            _buttonMorakhasiPelakTypeLabel1BackgroundColor = Color.White;
            _buttonMorakhasiPelakTypeLabel1TextColor = Color.Black;

            OnPropertyChanged("ButtonMorakhasiPelakTypeLabel0BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiPelakTypeLabel0TextColor");
            OnPropertyChanged("ButtonMorakhasiPelakTypeLabel1BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiPelakTypeLabel1TextColor");
        }

        private void ButtonMorakhasiDayTypeLabel0_OnClicked()
        {
            ButtonMorakhasiDayType = 0;
            _buttonMorakhasiDayTypeLabel0BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMorakhasiDayTypeLabel0TextColor = Color.White;
            _buttonMorakhasiDayTypeLabel1BackgroundColor = Color.White;
            _buttonMorakhasiDayTypeLabel1TextColor = Color.Black;
            _buttonMorakhasiDayTypeLabel2BackgroundColor = Color.White;
            _buttonMorakhasiDayTypeLabel2TextColor = Color.Black;

            OnPropertyChanged("ButtonMorakhasiDayTypeLabel0BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel0TextColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel1BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel1TextColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel2BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel2TextColor");
        }

        private void ButtonMorakhasiDayTypeLabel1_OnClicked()
        {
            ButtonMorakhasiDayType = 1;
            _buttonMorakhasiDayTypeLabel1BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMorakhasiDayTypeLabel1TextColor = Color.White;
            _buttonMorakhasiDayTypeLabel0BackgroundColor = Color.White;
            _buttonMorakhasiDayTypeLabel0TextColor = Color.Black;
            _buttonMorakhasiDayTypeLabel2BackgroundColor = Color.White;
            _buttonMorakhasiDayTypeLabel2TextColor = Color.Black;

            OnPropertyChanged("ButtonMorakhasiDayTypeLabel1BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel1TextColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel0BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel0TextColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel2BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel2TextColor");
        }

        private void ButtonMorakhasiDayTypeLabel2_OnClicked()
        {
            ButtonMorakhasiDayType = 2;
            _buttonMorakhasiDayTypeLabel2BackgroundColor = Color.FromHex("#3d7dad");
            _buttonMorakhasiDayTypeLabel2TextColor = Color.White;
            _buttonMorakhasiDayTypeLabel0BackgroundColor = Color.White;
            _buttonMorakhasiDayTypeLabel0TextColor = Color.Black;
            _buttonMorakhasiDayTypeLabel1BackgroundColor = Color.White;
            _buttonMorakhasiDayTypeLabel1TextColor = Color.Black;

            OnPropertyChanged("ButtonMorakhasiDayTypeLabel2BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel2TextColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel0BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel0TextColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel1BackgroundColor");
            OnPropertyChanged("ButtonMorakhasiDayTypeLabel1TextColor");
        }

        #endregion

    }
}
