﻿namespace ArvandCarApp.DependencyInterfaces
{
    public interface IFileService
    {
        string GetLocalFilePath(string filename);
    }
}
