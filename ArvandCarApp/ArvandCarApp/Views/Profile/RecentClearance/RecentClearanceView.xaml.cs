﻿using System;
using ArvandCarApp.Models;
using Syncfusion.ListView.XForms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.RecentClearance
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecentClearanceView
    {
        public RecentClearanceView()
        {
            InitializeComponent();

            #region BackPageButton

            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) => await Navigation.PopAsync();
            BackPageButton.GestureRecognizers.Add(back);

            #endregion

        }

        private void OnFilterTextChanged(object sender, TextChangedEventArgs e)
        {
            if (RecentClearancItems.DataSource != null)
            {
                RecentClearancItems.DataSource.Filter = FilterContacts;
                RecentClearancItems.DataSource.RefreshFilter();
            }
        }

        private bool FilterContacts(object obj)
        {
            if (SearchInList?.Text == null)
                return true;

            return obj is RecentClearanceModel model && model.Fullname.ToLower().Contains(SearchInList.Text.ToLower());
        }

        private async void RecentClearancItems_OnSelectionChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            var selected = RecentClearancItems.SelectedItem;
            if (selected != null)
            {
                RecentClearancItems.SelectedItem = null;

                var model = (RecentClearanceModel)selected;
                var text = await DisplayActionSheet($"ارتباط با {model.Fullname}", "بستن", null, $"تماس: {model.Mobile}", $"ایمیل: {model.Email}");
                if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
                {
                    if (text.Equals($"تماس: {model.Mobile}"))
                    {
                        Device.OpenUri(new Uri($"tel:{model.Mobile}"));
                    }
                    if (text.Equals($"ایمیل: {model.Email}"))
                    {
                        Device.OpenUri(new Uri($"mailto:{model.Email}"));
                    }
                }
            }
        }
    }
}