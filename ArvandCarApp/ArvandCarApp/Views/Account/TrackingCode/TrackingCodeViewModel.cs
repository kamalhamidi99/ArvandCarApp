﻿using System.Collections.ObjectModel;
using ArvandCarApp.Custom;
using ArvandCarApp.DependencyInterfaces;
using Xamarin.Forms;

namespace ArvandCarApp.Views.Account.TrackingCode
{
    public class TrackingCodeViewModel 
    {
        public ObservableCollection<int> SelectedDate { get; set; }
        public ObservableCollection<object> ItemsSource { get; set; }

        public TrackingCodeViewModel()
        {
            var dateService = DependencyService.Get<IPersianDateService>();
            var dateNow = dateService.GetPersianDateArray();

            var nguid = new CustomDatePicker();
            ItemsSource = nguid.Date;
            SelectedDate = new ObservableCollection<int>()
            {
                int.Parse(dateNow[0].Substring(dateNow[0].Length - 2, 2)),
                int.Parse(dateNow[1]) - 1,
                int.Parse(dateNow[2]) - 1
            };
        }

    }
}