﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Helpers;
using SQLite;
using Xamarin.Forms;

namespace ArvandCarApp.Services.Login
{
    public class LoginService
    {
        readonly SQLiteAsyncConnection _database;

        public LoginService()
        {
            _database = App.SqLiteAsyncConnection;
        }

        public async Task<string> SendAsync(string username, string password)
        {
            var result = await HttpClientHelper.SendAndReadAsync(httpMethod: HttpMethod.Post,
                url: $"{ApiUrlHelper.Url}/api/login", username: username, password: password);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                return string.Empty;
            }

            return "امکان ورود به سیستم وجود ندارد";
        }

        public async Task<bool> AddAsync(LoginModel model)
        {
            try
            {
                var existModel = await GetAsync();
                if (existModel.Id == 0) // add
                {
                    
                    await _database.CreateTableAsync<LoginModel>();

                    model.Id = 1;
                    var resultAdd = await _database.InsertAsync(model);

                    return resultAdd > 0;
                }

                // update
                return await UpdateAsync(existModel);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                return false;
            }
        }

        public async Task<bool> UpdateAsync(LoginModel model)
        {
            model.Password = model.Password;
            model.Username = model.Username;
            var resultUpdate = await _database.UpdateAsync(model);

            return resultUpdate > 0;
        }

        public async Task DeleteAsync()
        {
            if (await TableExistAsync())
                await _database.DropTableAsync<LoginModel>();
        }

        public async Task<LoginModel> GetAsync()
        {
            if (await TableExistAsync())
                return await _database.Table<LoginModel>().Where(x => x.Id == 1).FirstOrDefaultAsync();

            return new LoginModel();
        }

        public async Task<bool> IsLoginAsync()
        {
            var model = await GetAsync();
            if (model.Id == 0)
                return false;
            
            var result = await SendAsync(model.Username, model.Password);

            return string.IsNullOrWhiteSpace(result);
        }

        public bool IsLogin()
        {
            var db = DependencyService.Get<ISqLiteService>().DbConnection();

            var resultGet = db.ExecuteScalar<string>($"SELECT name FROM sqlite_master WHERE type='table' AND name='{nameof(LoginModel)}';");
            if (string.IsNullOrWhiteSpace(resultGet))
                return false;

            var model = db.Table<LoginModel>().FirstOrDefault();
            if (model == null || model.Id == 0)
                return false;

            var result = HttpClientHelper.SendAndRead(httpMethod: HttpMethod.Post,
                url: $"{ApiUrlHelper.Url}/api/login", username: model.Username, password: model.Password);
            if (result.StatusCode == HttpStatusCode.OK)
                return true;

            return false;
        }

        private async Task<bool> TableExistAsync()
        {
            var tableExistsQuery = $"SELECT name FROM sqlite_master WHERE type='table' AND name='{nameof(LoginModel)}';";
            var result = await _database.ExecuteScalarAsync<string>(tableExistsQuery);
            if (result != null)
                return true;

            return false;
        }

    }
}
