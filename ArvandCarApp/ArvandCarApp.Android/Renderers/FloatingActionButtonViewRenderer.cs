using System;
using System.IO;
using Android.Views;
using Android.Widget;
using ArvandCarApp.Custom.FabBtn;
using ArvandCarApp.Droid.Renderers;
using com.refractored.fab;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(FloatingActionButtonView), typeof(FloatingActionButtonViewRenderer))]
namespace ArvandCarApp.Droid.Renderers
{
    public class FloatingActionButtonViewRenderer : ViewRenderer<FloatingActionButtonView, FrameLayout>
    {
        private const int MarginDips = 16;
        private const int FabHeightNormal = 56;
        private const int FabHeightMini = 40;
        private const int FabFrameHeightWithPadding = (MarginDips * 2) + FabHeightNormal;
        private const int FabFrameWidthWithPadding = (MarginDips * 2) + FabHeightNormal;
        private const int FabMiniFrameHeightWithPadding = (MarginDips * 2) + FabHeightMini;
        private const int FabMiniFrameWidthWithPadding = (MarginDips * 2) + FabHeightMini;
        private readonly Android.Content.Context _context;
        private readonly FloatingActionButton _fab;

        public FloatingActionButtonViewRenderer()
        {
            _context = Forms.Context;

            float d = _context.Resources.DisplayMetrics.Density;
            var margin = (int)(MarginDips * d); // margin in pixels

            _fab = new FloatingActionButton(_context);
            var lp = new FrameLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.WrapContent)
            {
                Gravity = GravityFlags.CenterVertical | GravityFlags.CenterHorizontal,
                LeftMargin = margin,
                TopMargin = margin,
                BottomMargin = margin,
                RightMargin = margin
            };
            _fab.LayoutParameters = lp;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<FloatingActionButtonView> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
                return;

            if (e.OldElement != null)
                e.OldElement.PropertyChanged -= HandlePropertyChanged;

            if (Element != null)
            {
                //UpdateContent ();
                Element.PropertyChanged += HandlePropertyChanged;
            }

            Element.Show = Show;
            Element.Hide = Hide;

            SetFabImage(Element.ImageName);
            SetFabSize(Element.Size);

            _fab.ColorNormal = Element.ColorNormal.ToAndroid();
            _fab.ColorPressed = Element.ColorPressed.ToAndroid();
            _fab.ColorRipple = Element.ColorRipple.ToAndroid();
            _fab.HasShadow = Element.HasShadow;
            _fab.Click += Fab_Click;

            var frame = new FrameLayout(_context);
            frame.RemoveAllViews();
            frame.AddView(_fab);

            SetNativeControl(frame);
        }

        public void Show(bool animate = true)
        {
            _fab.Show(animate);
        }

        public void Hide(bool animate = true)
        {
            _fab.Hide(animate);
        }

        void HandlePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Content")
            {
                Tracker.UpdateLayout();
            }
            else if (e.PropertyName == FloatingActionButtonView.ColorNormalProperty.PropertyName)
            {
                _fab.ColorNormal = Element.ColorNormal.ToAndroid();
            }
            else if (e.PropertyName == FloatingActionButtonView.ColorPressedProperty.PropertyName)
            {
                _fab.ColorPressed = Element.ColorPressed.ToAndroid();
            }
            else if (e.PropertyName == FloatingActionButtonView.ColorRippleProperty.PropertyName)
            {
                _fab.ColorRipple = Element.ColorRipple.ToAndroid();
            }
            else if (e.PropertyName == FloatingActionButtonView.ImageNameProperty.PropertyName)
            {
                SetFabImage(Element.ImageName);
            }
            else if (e.PropertyName == FloatingActionButtonView.SizeProperty.PropertyName)
            {
                SetFabSize(Element.Size);
            }
            else if (e.PropertyName == FloatingActionButtonView.HasShadowProperty.PropertyName)
            {
                _fab.HasShadow = Element.HasShadow;
            }
        }

        void SetFabImage(string imageName)
        {
            if (!string.IsNullOrWhiteSpace(imageName))
            {
                try
                {
                    var drawableNameWithoutExtension = Path.GetFileNameWithoutExtension(imageName);
                    var resources = _context.Resources;
                    var imageResourceName = resources.GetIdentifier(drawableNameWithoutExtension, "drawable", _context.PackageName);
                    _fab.SetImageBitmap(Android.Graphics.BitmapFactory.DecodeResource(_context.Resources, imageResourceName));
                }
                catch (Exception ex)
                {
                    throw new FileNotFoundException("There was no Android Drawable by that name.", ex);
                }
            }
        }

        void SetFabSize(FloatingActionButtonSize size)
        {
            if (size == FloatingActionButtonSize.Mini)
            {
                _fab.Size = FabSize.Mini;
                Element.WidthRequest = FabMiniFrameWidthWithPadding;
                Element.HeightRequest = FabMiniFrameHeightWithPadding;
            }
            else
            {
                _fab.Size = FabSize.Normal;
                Element.WidthRequest = FabFrameWidthWithPadding;
                Element.HeightRequest = FabFrameHeightWithPadding;
            }
        }

        void Fab_Click(object sender, EventArgs e)
        {
            var clicked = Element.Clicked;
            if (Element != null)
            {
                clicked?.Invoke(sender, e);
            }
        }
    }
}

