﻿using System;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Services.RegisterNewCar;
using ArvandCarApp.Views.About;
using ArvandCarApp.Views.Account.Login;
using ArvandCarApp.Views.Profile.Gps;
using ArvandCarApp.Views.Profile.History;
using ArvandCarApp.Views.Profile.Info;
using ArvandCarApp.Views.Profile.Law;
using ArvandCarApp.Views.Profile.LeftZone;
using ArvandCarApp.Views.Profile.License;
using ArvandCarApp.Views.Profile.LicensedCompany;
using ArvandCarApp.Views.Profile.RecentClearance;
using ArvandCarApp.Views.Profile.Vacation;
using Plugin.Share;
using Plugin.Share.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.Main
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainView
    {
        private int _closeApp;

        public MainView()
        {
            InitializeComponent();

            #region ShowRightMenu

            var tap = new TapGestureRecognizer();
            tap.Tapped += (s,e) => NavigationDrawer.ToggleDrawer();
            ShowRightMenu.GestureRecognizers.Add(tap);

            #endregion

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var data = await App.FullInfoService.GetAsync();
            if (data.Id > 0)
            {
                RightMenuName.Text = data.OwnerName;
                RightMenuMeliCode.Text = data.OwnerMeliCode;
                RightMenuRegiterCode.Text = data.CarRegisterCode;
            }
            else
            {
                Navigation.InsertPageBefore(new LoginView(), Navigation.NavigationStack[0]);
                await Navigation.PopToRootAsync();
            }
        }

        protected override bool OnBackButtonPressed()
        {
            if (_closeApp == 1)
            {
                Navigation.InsertPageBefore(new MainView(), Navigation.NavigationStack[0]);
                Navigation.PopToRootAsync();
            }
            else
            {
                _closeApp = 1;
                var notificator = DependencyService.Get<IMessageToast>();
                notificator.ShortAlert("برای خروج مجددا دکمه بازگشت را بزنید");
                return true;
            }

            return base.OnBackButtonPressed();
        }

        #region GoTo Clicked

        private async void GoToVacation_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new VacationMainView());

        private async void GoToHistory_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new HistoryView());

        private async void GoToInfo_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new InfoView());

        private async void GoToLeftZone_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new LeftZoneMainView());

        private async void GoToLicense_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new LicenseMainView());

        private async void GoToGps_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new GpsView());

        private async void GoToLaw_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new LawView());

        private async void GoToCompanys_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new LicensedCompanyView());

        private async void GoToClearance_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new RecentClearanceView());

        #endregion

        private void GoToWebSiteButton_OnClicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("http://car.arvandfreezone.com"));
            NavigationDrawer.ToggleDrawer();
        }

        private async void ShareButton_OnClicked(object sender, EventArgs e)
        {
            await CrossShare.Current.Share(new ShareMessage()
            {
                Title = "اروند خودرو",
                Text = "اپلیکیشن سامانه خودرو منطقه آزاد اروند",
                Url = "https://cafebazaar.ir/app/com.arvand.car/?l=fa"
            });
            NavigationDrawer.ToggleDrawer();
        }

        private async void AboutButton_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AboutView());
            NavigationDrawer.ToggleDrawer();
        }

        private async void LogoutButton_OnClicked(object sender, EventArgs e)
        {
            await App.LoginService.DeleteAsync();
            await App.FullInfoService.DeleteAsync();
            await App.NumberValidationService.DeleteAsync();
            new RegisterNewCarService().Delete();

            Navigation.InsertPageBefore(new LoginView(), Navigation.NavigationStack[0]);
            await Navigation.PopToRootAsync();

            NavigationDrawer.ToggleDrawer();
        }

        //private void Sooooon()
        //{
        //    var notificator = DependencyService.Get<IMessageToast>();
        //    notificator.ShortAlert("به زودی");
        //}

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            var notificator = DependencyService.Get<ICafeBazarService>();
            var result = notificator.SendMessage();
            if (!string.IsNullOrWhiteSpace(result))
            {
                await DisplayAlert("خطا", result, "بستن");
            }
        }
    }
}