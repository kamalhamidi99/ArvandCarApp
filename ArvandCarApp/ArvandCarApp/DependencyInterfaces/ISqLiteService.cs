﻿using SQLite;

namespace ArvandCarApp.DependencyInterfaces
{
    public interface ISqLiteService
    {
        SQLiteConnection DbConnection();
    }
}
