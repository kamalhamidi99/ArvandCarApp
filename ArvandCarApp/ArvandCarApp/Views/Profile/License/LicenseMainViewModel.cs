﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ArvandCarApp.Services.License;
using ArvandCarApp.ViewModels;
using Xamarin.Forms;

namespace ArvandCarApp.Views.Profile.License
{
    public class LicenseMainViewModel : BaseViewModel
    {
        #region Properties

        public Command LoadItemsCommand { get; set; }

        private ObservableCollection<LicenseModel> _licenseModels;
        public ObservableCollection<LicenseModel> LicenseModels
        {
            get => _licenseModels;
            set => SetProperty(ref _licenseModels, value);
        }

        private bool _loadingWait;
        public bool LoadingWait
        {
            get => _loadingWait;
            set => SetProperty(ref _loadingWait, value);
        }

        private bool _noData;
        public bool NoData
        {
            get => _noData;
            set => SetProperty(ref _noData, value);
        }

        #endregion

        #region Constructor

        public LicenseMainViewModel()
        {
            LicenseModels = new ObservableCollection<LicenseModel>();
            LoadItemsCommand = new Command(async () => await LoadMoreData());
        }

        public async Task LoadMoreData()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            LoadingWait = true;
            NoData = false;

            await Task.Delay(100);

            var allLicenseModels = await App.LicenseService.LoadAsync();

            if (allLicenseModels.Any())
            {
                LicenseModels.Clear();

                foreach (var model in FixData(allLicenseModels))
                {
                    LicenseModels.Add(model);
                }
            }

            LoadingWait = false;
            IsBusy = true;
        }


        #endregion

        #region FixData

        private static IList<LicenseModel> FixData(IList<LicenseModel> data)
        {
            foreach (var item in data)
            {
                if (item.IsPaied || item.IsFirstYear) // سبز
                {
                    item.Color = "DarkCyan";
                }
                else  // قرمز
                {
                    item.Color = "DarkRed";
                }

                item.ShowPaying = !item.IsPaied;
                item.ShowPaied = item.IsPaied;
                item.AmountPaied = !string.IsNullOrWhiteSpace(item.AmountPaied) ? item.AmountPaied : "سال اول";
                item.StartDateStr = !string.IsNullOrWhiteSpace(item.StartDateStr) ? item.StartDateStr : "-";
                item.EndDateStr = !string.IsNullOrWhiteSpace(item.EndDateStr) ? item.EndDateStr : "-";
            }

            return data;
        }

        #endregion

    }
}
