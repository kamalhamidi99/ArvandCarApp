using System;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Droid.DependencyServices;
using Java.Util;
using Xamarin.Forms;

[assembly: Dependency(typeof(LanguageService))]
namespace ArvandCarApp.Droid.DependencyServices
{
    public class LanguageService : ILanguageService
    {
        public Locale Info { get; set; }

        public LanguageService()
        {
            Info = Locale.Default;
        }

        private string _language;
        public string Language
        {
            get => _language;
            set
            {
                if (value == null) throw new ArgumentNullException(nameof(value));
                _language = Info.Language.ToLower();
            }
        }

        private string _country;
        public string Country
        {
            get => _country;
            set
            {
                if (value == null) throw new ArgumentNullException(nameof(value));
                _country = Info.Country.ToLower();
            }
        }

    }
}