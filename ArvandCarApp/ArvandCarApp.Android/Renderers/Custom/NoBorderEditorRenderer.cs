using System;
using System.ComponentModel;
using Android.App;
using Android.Views;
using ArvandCarApp.Custom;
using ArvandCarApp.Droid.Renderers.Custom;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Application = Xamarin.Forms.Application;
using Color = Android.Graphics.Color;

[assembly: ExportRenderer(typeof(NoBorderEditor), typeof(EditorCtrlRenderer))]
namespace ArvandCarApp.Droid.Renderers.Custom
{
    public class EditorCtrlRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                Control.SetBackgroundColor(Color.Transparent);

                var element = e.NewElement as NoBorderEditor;
                if (element != null) Control.Hint = element.Placeholder;

                if (element != null && element.TextAlignment.ToLower() == "center")
                {
                    Control.TextAlignment = Android.Views.TextAlignment.Center;
                    Control.Gravity = GravityFlags.Center;
                }
                if (element != null && element.TextAlignment.ToLower() == "start")
                {
                    Control.TextAlignment = Android.Views.TextAlignment.TextStart;
                    Control.Gravity = GravityFlags.Start;
                }
                if (element != null && element.TextAlignment.ToLower() == "end")
                {
                    Control.TextAlignment = Android.Views.TextAlignment.TextEnd;
                    Control.Gravity = GravityFlags.End;
                }
            }

            Control.FocusChange += Control_FocusChange;
        }

        void Control_FocusChange(object sender, FocusChangeEventArgs e)
        {
            if (e.HasFocus)
            {
                if (Application.Current.Properties.ContainsKey("count"))
                {
                    int ct = Convert.ToInt32(Application.Current.Properties["count"]);
                    if (ct > 1)
                    {
                        var activity = Forms.Context as Activity;
                        activity?.Window.SetSoftInputMode(SoftInput.AdjustPan);
                    }
                    else
                    {
                        var activity = Forms.Context as Activity;
                        activity?.Window.SetSoftInputMode(SoftInput.AdjustResize);
                    }
                }
            }
            else
            {
                var activity = Forms.Context as Activity;
                activity?.Window.SetSoftInputMode(SoftInput.AdjustNothing);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == NoBorderEditor.PlaceholderProperty.PropertyName)
            {
                var element = Element as NoBorderEditor;
                if (element != null) Control.Hint = element.Placeholder;

                if (element != null && element.TextAlignment.ToLower() == "center")
                {
                    Control.TextAlignment = Android.Views.TextAlignment.Center;
                    Control.Gravity = GravityFlags.Center;
                }
                if (element != null && element.TextAlignment.ToLower() == "start")
                {
                    Control.TextAlignment = Android.Views.TextAlignment.TextStart;
                    Control.Gravity = GravityFlags.Start;
                }
                if (element != null && element.TextAlignment.ToLower() == "end")
                {
                    Control.TextAlignment = Android.Views.TextAlignment.TextEnd;
                    Control.Gravity = GravityFlags.End;
                }
            }
        }
    }
}