﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ArvandCarApp.Helpers;
using Newtonsoft.Json;

namespace ArvandCarApp.Services.License
{
    public class LicenseService
    {

        public async Task<IList<LicenseModel>> LoadAsync()
        {
            var httpCarNames =
                await HttpClientHelper.SendAndReadAsync(HttpMethod.Post, $"{ApiUrlHelper.Url}/api/carlicense/all", isAuthorization: true);
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrWhiteSpace(httpCarNames.ResponseMessage))
                {
                    var data = JsonConvert.DeserializeObject<IList<LicenseModel>>(httpCarNames.ResponseMessage);
                    if (data != null && data.Any())
                    {
                        return data;
                    }
                }
            }

            return new List<LicenseModel>();
        }

    }
}
