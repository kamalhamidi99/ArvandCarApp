using System;
using Android.Views;
using Android.Widget;
using ArvandCarApp.Custom;
using ArvandCarApp.Droid.Renderers.Custom;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(MyDropDownPicker), typeof(CustomDropDownPickerRenderer))]
namespace ArvandCarApp.Droid.Renderers.Custom
{
    public class CustomDropDownPickerRenderer : ViewRenderer<Picker, Spinner>
    {
        Picker _picker;

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || Element == null)
            {
                return;
            }
            _picker = e.NewElement;
            var scaleNames = e.NewElement.Items;
            var spinner = new Spinner(this.Context)
            {
                TextDirection = TextDirection.Rtl,
                TextAlignment = Android.Views.TextAlignment.ViewEnd,
            };

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            //spinner.SetSelection(e.NewElement.SelectedIndex);

            var scaleAdapter = new ArrayAdapter<string>(this.Context, Android.Resource.Layout.SimpleSpinnerItem, scaleNames);

            scaleAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);

            spinner.Adapter = scaleAdapter;

            base.SetNativeControl(spinner);
        }

        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            _picker.SelectedIndex = (e.Position);
        }
    }
}