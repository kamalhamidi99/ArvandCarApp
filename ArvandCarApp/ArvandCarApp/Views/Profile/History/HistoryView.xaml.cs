﻿using ArvandCarApp.Services.History;
using Syncfusion.ListView.XForms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.History
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryView
    {
        private readonly HistoryViewModel _viewModel;

        public HistoryView()
        {
            InitializeComponent();

            BindingContext = _viewModel = new HistoryViewModel()
            {
                Navigation = Navigation,
            };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadItemsCommand.Execute(null);
        }

        #region HistoryItems_OnSelectionChanged

        private async void HistoryItems_OnSelectionChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            var selected = HistoryItems.SelectedItem;
            if (selected != null)
            {
                HistoryItems.SelectedItem = null;

                var model = (HistoryModel)selected;
                await DisplayAlert("پیام", model.Message, "بستن");
            }
        }

        #endregion
    }
}