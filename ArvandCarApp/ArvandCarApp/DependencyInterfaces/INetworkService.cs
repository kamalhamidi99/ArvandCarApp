﻿namespace ArvandCarApp.DependencyInterfaces
{
    public interface INetworkService
    {
        bool IsOnline();
        bool IsWifi();
        string GetNetworkType();
    }
}