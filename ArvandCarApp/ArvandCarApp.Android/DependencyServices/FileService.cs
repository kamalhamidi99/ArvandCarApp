﻿using System;
using System.IO;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Droid.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileService))]
namespace ArvandCarApp.Droid.DependencyServices
{
    public class FileService : IFileService
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}
