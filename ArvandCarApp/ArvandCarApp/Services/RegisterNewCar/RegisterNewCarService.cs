﻿using System;
using System.Diagnostics;
using ArvandCarApp.DependencyInterfaces;
using SQLite;
using Xamarin.Forms;

namespace ArvandCarApp.Services.RegisterNewCar
{
    public class RegisterNewCarService
    {
        private readonly SQLiteConnection _db;

        public RegisterNewCarService()
        {
            var sqLiteDbService = DependencyService.Get<ISqLiteService>();
            _db = sqLiteDbService.DbConnection();
        }

        public bool StoreData(RegisterNewCarModel model)
        {
            try
            {
                var registerNewDataModel = Get();
                if (registerNewDataModel.Id == 0) // add
                {
                    _db.CreateTable<RegisterNewCarModel>();

                    model.Id = 1;
                    var resultAdd = _db.Insert(model);

                    return resultAdd > 0;
                }

                // update
                registerNewDataModel.Ishaghighi = model.Ishaghighi;
                registerNewDataModel.IsofficGoverMent = model.IsofficGoverMent;
                registerNewDataModel.OwnerMeliCode = model.OwnerMeliCode;
                registerNewDataModel.OwnerName = model.OwnerName;
                registerNewDataModel.OwnerFather = model.OwnerFather;
                registerNewDataModel.OwnerShenasname = model.OwnerShenasname;
                registerNewDataModel.OwnerSadere = model.OwnerSadere;
                registerNewDataModel.OwnerBirthdayStrYear = model.OwnerBirthdayStrYear;
                registerNewDataModel.OwnerBirthdayStrMonth = model.OwnerBirthdayStrMonth;
                registerNewDataModel.OwnerBirthdayStrDay = model.OwnerBirthdayStrDay;
                registerNewDataModel.OwnerSex = model.OwnerSex;
                registerNewDataModel.OwnerTell = model.OwnerTell;
                registerNewDataModel.OwnerMobile = model.OwnerMobile;
                registerNewDataModel.OwnerAddress = model.OwnerAddress;
                registerNewDataModel.FkCarOption = model.FkCarOption;
                registerNewDataModel.IhavePelakNumber = model.IhavePelakNumber;
                registerNewDataModel.FkCarNAme = model.FkCarNAme;
                registerNewDataModel.FkCarTip = model.FkCarTip;
                registerNewDataModel.FkCarSystem = model.FkCarSystem;
                registerNewDataModel.CarYearCreate = model.CarYearCreate;
                registerNewDataModel.CarYearModle = model.CarYearModle;
                registerNewDataModel.FkCarCountry = model.FkCarCountry;
                registerNewDataModel.FkCarColour = model.FkCarColour;
                registerNewDataModel.CarDoorCounts = model.CarDoorCounts;
                registerNewDataModel.CarSeatsCounts = model.CarSeatsCounts;
                registerNewDataModel.CarMotorCode = model.CarMotorCode;
                registerNewDataModel.CarShassisNumber = model.CarShassisNumber;
                registerNewDataModel.CarCountryType = model.CarCountryType;
                registerNewDataModel.CarTrasiteType = model.CarTrasiteType;
                var resultUpdate = _db.Update(registerNewDataModel);

                return resultUpdate > 0;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
                return false;
            }
        }

        public void Delete()
        {
            if (TableExist())
                _db.DropTable<RegisterNewCarModel>();
        }

        public RegisterNewCarModel Get()
        {
            if (!TableExist())
                return new RegisterNewCarModel();

            return _db.Table<RegisterNewCarModel>().FirstOrDefault();
        }

        private bool TableExist()
        {
            var tableExistsQuery = $"SELECT name FROM sqlite_master WHERE type='table' AND name='{nameof(RegisterNewCarModel)}';";
            var result = _db.ExecuteScalar<string>(tableExistsQuery);
            if (result != null)
                return true;

            return false;
        }

    }
}
