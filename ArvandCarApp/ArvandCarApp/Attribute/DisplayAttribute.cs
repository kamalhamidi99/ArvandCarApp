﻿using System;

namespace ArvandCarApp.Attribute
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class DisplayAttribute : System.Attribute
    {
        public string Name1 { get; }
        public string Name2 { get; }

        public DisplayAttribute(string name1, string name2 = null)
        {
            Name1 = name1;
            Name2 = name2;
        }
    }
}