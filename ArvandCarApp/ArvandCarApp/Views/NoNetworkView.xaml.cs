﻿using System;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Views.Account.Login;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoNetworkView
    {
        public NoNetworkView()
        {
            InitializeComponent();
        }

        private void CheckNetwork(object sender, EventArgs e)
        {
            var networkInfo = DependencyService.Get<INetworkService>();
            if (networkInfo != null)
            {
                if (networkInfo.IsOnline())
                {
                    Navigation.InsertPageBefore(new LoginView(), Navigation.NavigationStack[0]);
                    Navigation.PopToRootAsync();
                }
                else
                {
                    DisplayAlert("وضعیت شبکه", "ارتباط شما با اینترنت قطع است.", "بستن");
                }
            }
            else
            {
                DisplayAlert("وضعیت شبکه", "ارتباط شما با اینترنت قطع است.", "بستن");
            }
        }
    }
}
