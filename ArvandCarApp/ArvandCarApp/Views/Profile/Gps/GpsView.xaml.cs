﻿using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.Gps
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GpsView
    {
        private readonly GpsViewModel _viewModel;

        public GpsView()
        {
            InitializeComponent();

            BindingContext = _viewModel = new GpsViewModel()
            {
                Navigation = Navigation,
            };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadItemsCommand.Execute(null);
        }
    }
}