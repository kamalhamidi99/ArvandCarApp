﻿using SQLite;

namespace ArvandCarApp.Services.LeftZone.LeftZoneInfo
{
    public class LeftZoneInfoModel
    {
        [PrimaryKey]
        public long Id { get; set; }

        public int LeftZoneCalculateTotalDay { get; set; }
        public long LeftZoneCalculateTotalDayInMorakhasi { get; set; }
        public int LeftZoneCalculateTotalDayPaied { get; set; }
        public int LeftZoneCalculateTotalPrice { get; set; }
        public int LeftZoneGroupCarId { get; set; }
        public string LeftZoneGroupCarTitle { get; set; }
        public int LeftZoneGroupCarYearlyFixAmount { get; set; }
        public LeftZoneGroupCarType LeftZoneGroupCarType { get; set; }
        public bool LeftZoneGroupCarPaymentOldYearNotPay { get; set; }
        public bool LeftZonePaymentYearlyFixAmountIsOk { get; set; }
        public int LeftZonePaymentYearlyFixAmountAmountPaied { get; set; }
        public int LeftZonePaymentYearlyFixAmountForPersianYear { get; set; }
    }


    public enum LeftZoneGroupCarType
    {
        /// <summary>
        /// محاسبه روزانه عوارض
        /// </summary>
        Normal,

        /// <summary>
        /// Gps مشکل دارد
        /// </summary>
        GpsError,

        /// <summary>
        /// بدون عوارض
        /// </summary>
        Free,

        /// <summary>
        /// عوارض با مبلغ ثابت
        /// </summary>
        YearlyFixAmount,
    }

}
