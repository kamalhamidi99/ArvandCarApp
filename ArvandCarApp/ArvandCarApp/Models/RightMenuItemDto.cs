﻿namespace ArvandCarApp.Models
{
    class RightMenuItemDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Icon { get; set; }
    }
}
