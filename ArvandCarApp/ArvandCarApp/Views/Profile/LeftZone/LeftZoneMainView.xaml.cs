﻿using System;
using System.Text;
using System.Threading.Tasks;
using ArvandCarApp.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.LeftZone
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LeftZoneMainView
    {
        private readonly LeftZoneMainViewModel _viewModel;

        public LeftZoneMainView()
        {
            InitializeComponent();

            _viewModel = new LeftZoneMainViewModel();
            BindingContext = _viewModel;

            #region BackPageButton

            var back = new TapGestureRecognizer();
            back.Tapped += BackPageButton_Clicked;
            BackPageButton.GestureRecognizers.Add(back);

            #endregion

            #region DetailsBox

            var details = new TapGestureRecognizer();
            details.Tapped += async (s, e) => await GoToWeb_Clicked();
            DetailsBox.GestureRecognizers.Add(details);

            #endregion

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadItemsCommand.Execute(null);
        }

        #region BackPageButton Method

        private async void BackPageButton_Clicked(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        #endregion

        #region GoToWeb

        private async Task GoToWeb_Clicked()
        {
            var data = await App.LeftZoneInfoService.LoadAsync();
            if (data?.LeftZoneCalculateTotalPrice <= 0)
                return;

            var login = await App.LoginService.GetAsync();
            var plainTextBytes =
                Encoding.UTF8.GetBytes(login.Username + "-" + login.Password);
            Device.OpenUri(new Uri(ApiUrlHelper.Url + "/WebService/MobileLeftZone/Payment?q=" +
                                   Convert.ToBase64String(plainTextBytes)));
        }

        #endregion

    }
}