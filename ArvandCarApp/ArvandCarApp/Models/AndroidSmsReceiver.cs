﻿using Xamarin.Forms;

namespace ArvandCarApp.Models
{
    public class AndroidSmsReceiver
    {
        public AndroidSmsReceiver()
        {
            SubscribeToOtpReceiving();
        }

        private void SubscribeToOtpReceiving()
        {
            MessagingCenter.Subscribe<AndroidSmsReceiver, string>(this, "OtpReceived", (sender, code) =>
            {
                App.SmsMobileNumberValidate = code;
            });
        }
    }
}