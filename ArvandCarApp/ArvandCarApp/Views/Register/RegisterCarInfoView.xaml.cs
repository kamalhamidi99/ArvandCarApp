﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using ArvandCarApp.DependencyInterfaces;
using ArvandCarApp.Helpers;
using ArvandCarApp.Models.Api;
using ArvandCarApp.Services.RegisterNewCar;
using ArvandCarApp.Views.Account.Login;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Register
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterCarInfoView
    {
        private ApiBaseDto _baseData;

        private readonly bool _ishaghighi;
        private readonly bool _isofficGoverMent;
        private readonly string _ownerMeliCode;
        private readonly string _ownerName;
        private readonly string _ownerFather;
        private readonly string _ownerShenasname;
        private readonly string _ownerSadere;
        private readonly string _ownerBirthdayStr;
        private readonly byte _ownerSex;
        private readonly string _ownerTell;
        private readonly string _ownerMobile;
        private readonly string _ownerAddress;

        public RegisterCarInfoView(bool ishaghighi, bool isofficGoverMent, string ownerMeliCode, string ownerName, 
            string ownerFather, string ownerShenasname, string ownerSadere, string ownerBirthdayStr, 
            byte ownerSex, string ownerTell, string ownerMobile, string ownerAddress)
        {
            _ishaghighi = ishaghighi;
            _isofficGoverMent = isofficGoverMent;
            _ownerMeliCode = ownerMeliCode;
            _ownerName = ownerName;
            _ownerFather = ownerFather;
            _ownerShenasname = ownerShenasname;
            _ownerSadere = ownerSadere;
            _ownerBirthdayStr = ownerBirthdayStr;
            _ownerSex = ownerSex;
            _ownerTell = ownerTell;
            _ownerMobile = ownerMobile;
            _ownerAddress = ownerAddress;

            InitializeComponent();

            // back button
            var tgr = new TapGestureRecognizer();
            tgr.Tapped += (s, e) => Navigation.PopAsync();
            BackPage.GestureRecognizers.Add(tgr);

            var display = DependencyService.Get<IDisplayService>();
            StackLayoutContent.WidthRequest = display.Width;

            // load last data from storge
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var httpCarNames = await HttpClientHelper.SendAndReadAsync(HttpMethod.Get, $"{ApiUrlHelper.Url}/api/Data/Base");
            if (httpCarNames.StatusCode == HttpStatusCode.OK)
            {
                _baseData = JsonConvert.DeserializeObject<ApiBaseDto>(httpCarNames.ResponseMessage);

                LoadData();
            }
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            BackgroundLoading.IsVisible = true;
            LoadingMessage.IsVisible = true;

            var isOk = true;

            string fkCarOption = "";
            var ihavePelakNumber = EntryIhavePelakNumber.Text;

            var fkCarNAme = new ApiBaseDataDto();
            if (PickerCarName.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else fkCarNAme = _baseData.CarName.FirstOrDefault(x=>x.Title == PickerCarName.Text) ?? new ApiBaseDataDto();

            var fkCarTip = new ApiBaseDataDto();
            if (PickerTip.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else fkCarTip = fkCarNAme.Brigades.FirstOrDefault(x=>x.Title == PickerTip.Text) ?? new ApiBaseDataDto();

            var fkCarSystem = new ApiBaseDataDto();
            if (PickerSystem.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else fkCarSystem = _baseData.System.FirstOrDefault(x=>x.Title == PickerSystem.Text) ?? new ApiBaseDataDto();

            var carYearCreate = 0;
            if (PickerYearModle.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else carYearCreate = _baseData.YearOfConstruction.FirstOrDefault(x => x.Equals(int.Parse(PickerYearModle.Text)));

            var carYearModle = 0;
            if (PickerYearCreate.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else carYearModle = _baseData.YearOfConstruction.FirstOrDefault(x => x.Equals(int.Parse(PickerYearCreate.Text)));

            var fkCarCountry = new ApiBaseDataDto();
            if (PickerManufacturingCountry.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else fkCarCountry = _baseData.ManufacturingCountry.FirstOrDefault(x=>x.Title == PickerManufacturingCountry.Text) ?? new ApiBaseDataDto();

            var fkCarColour = new ApiBaseDataDto();
            if (PickerColor.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else fkCarColour = _baseData.Color.FirstOrDefault(x=>x.Title == PickerColor.Text) ?? new ApiBaseDataDto();

            var carDoorCounts = 0;
            if (PickerDoorCounts.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else carDoorCounts = _baseData.CarDoor.FirstOrDefault(x => x.Equals(int.Parse(PickerDoorCounts.Text)));

            var carSeatsCounts = 0;
            if (PickerSeatsCounts.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else carSeatsCounts = _baseData.CarSeat.FirstOrDefault(x => x.Equals(int.Parse(PickerSeatsCounts.Text)));

            if (string.IsNullOrWhiteSpace(EntryMotorCode.Text))
            {
                isOk = false;
            }
            var carMotorCode = EntryMotorCode.Text;

            if (string.IsNullOrWhiteSpace(EntryShassisNumber.Text))
            {
                isOk = false;
            }
            var carShassisNumber = EntryShassisNumber.Text;

            var carCountryType = new ApiBaseDataDto();
            if (PickerAreaBuild.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else carCountryType = _baseData.AreaBuild.FirstOrDefault(x=>x.Title == PickerAreaBuild.Text) ?? new ApiBaseDataDto();

            var carTrasiteType = new ApiBaseDataDto();
            if (PickerTrans.Text.Equals("انتخاب کنید"))
            {
                isOk = false;
            }
            else carTrasiteType = _baseData.Trans.FirstOrDefault(x=>x.Title == PickerTrans.Text) ?? new ApiBaseDataDto();

            if (isOk == false)
            {
                BackgroundLoading.IsVisible = false;
                LoadingMessage.IsVisible = false;

                await DisplayAlert("خطا", "لطفا اطلاعات ضروری را وارد کنید", "بستن");
                return;
            }

            var registerNewCarService = new RegisterNewCarService();
            var data = registerNewCarService.Get();
            data.FkCarNAme = fkCarNAme.Title;
            data.FkCarTip = fkCarTip.Title;
            data.FkCarSystem = fkCarSystem.Title;
            data.CarYearCreate = carYearCreate.ToString();
            data.CarYearModle = carYearModle.ToString();
            data.FkCarCountry = fkCarCountry.Title;
            data.FkCarColour = fkCarColour.Title;
            data.CarDoorCounts = carDoorCounts.ToString();
            data.CarSeatsCounts = carSeatsCounts.ToString();
            data.CarMotorCode = EntryMotorCode.Text;
            data.CarShassisNumber = EntryShassisNumber.Text;
            data.CarCountryType = carCountryType.Title;
            data.CarTrasiteType = carTrasiteType.Title;
            data.IhavePelakNumber = EntryIhavePelakNumber.Text;
            registerNewCarService.StoreData(data);
            
            var headers = new Dictionary<string, string>()
            {
                {"ishaghighi",_ishaghighi.ToString() },
                {"isofficGoverMent",_isofficGoverMent.ToString() },
                {"ownerMeliCode",_ownerMeliCode },
                {"ownerName",_ownerName },
                {"ownerFather",_ownerFather },
                {"ownerShenasname",_ownerShenasname },
                {"ownerSadere",_ownerSadere },
                {"ownerBirthdayStr",_ownerBirthdayStr },
                {"ownerSex",_ownerSex.ToString() },
                {"ownerTell",_ownerTell },
                {"ownerMobile",_ownerMobile },
                {"ownerAddress",_ownerAddress },
                {"fkCarOption",fkCarOption },
                {"ihavePelakNumber",ihavePelakNumber },
                {"fkCarNAme",fkCarNAme.Id.ToString() },
                {"fkCarTip",fkCarTip.Id.ToString() },
                {"fkCarSystem",fkCarSystem.Id.ToString() },
                {"carYearCreate",carYearCreate.ToString() },
                {"carYearModle",carYearModle.ToString() },
                {"fkCarCountry",fkCarCountry.Id.ToString() },
                {"fkCarColour",fkCarColour.Id.ToString() },
                {"carDoorCounts",carDoorCounts.ToString() },
                {"carSeatsCounts",carSeatsCounts.ToString() },
                {"carMotorCode",carMotorCode },
                {"carShassisNumber",carShassisNumber },
                {"carCountryType",carCountryType.Id.ToString() },
                {"carTrasiteType",carTrasiteType.Id.ToString() },
            };

            var result = await HttpClientHelper.SendAndReadAsync(httpMethod: HttpMethod.Post,
                url: $"{ApiUrlHelper.Url}/api/Register/NewCar", headers:headers);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                LoadingMessage.IsVisible = false;
                BackgroundLoading.Opacity = 1;
                SuccessMessage.IsVisible = true;
                SuccessMessageRegisterCode.Text = result.ResponseMessage;
                registerNewCarService.Delete();
            }
            else
            {
                LoadingMessage.IsVisible = false;
                FailureMessage.IsVisible = true;
                FailureMessageText.Text = result.ResponseMessage;
            }
        }

        private async void FailureButton_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private void LoadData()
        {
            var data = new RegisterNewCarService().Get();
            if (data.Id > 0)
            {
                if (!string.IsNullOrWhiteSpace(data.FkCarNAme))
                    PickerCarName.Text = data.FkCarNAme;

                if (!string.IsNullOrWhiteSpace(data.FkCarTip))
                    PickerTip.Text = data.FkCarTip;

                if (!string.IsNullOrWhiteSpace(data.FkCarSystem))
                    PickerSystem.Text = data.FkCarSystem;

                if (!string.IsNullOrWhiteSpace(data.CarYearCreate))
                    PickerYearCreate.Text = data.CarYearCreate;

                if (!string.IsNullOrWhiteSpace(data.CarYearModle))
                    PickerYearModle.Text = data.CarYearModle;

                if (!string.IsNullOrWhiteSpace(data.FkCarCountry))
                    PickerManufacturingCountry.Text = data.FkCarCountry;

                if (!string.IsNullOrWhiteSpace(data.FkCarColour))
                    PickerColor.Text = data.FkCarColour;

                if (!string.IsNullOrWhiteSpace(data.CarDoorCounts))
                    PickerDoorCounts.Text = data.CarDoorCounts;

                if (!string.IsNullOrWhiteSpace(data.CarSeatsCounts))
                    PickerSeatsCounts.Text = data.CarSeatsCounts;

                if (!string.IsNullOrWhiteSpace(data.CarMotorCode))
                    EntryMotorCode.Text = data.CarMotorCode;

                if (!string.IsNullOrWhiteSpace(data.CarShassisNumber))
                    EntryShassisNumber.Text = data.CarShassisNumber;

                if (!string.IsNullOrWhiteSpace(data.CarCountryType))
                    PickerAreaBuild.Text = data.CarCountryType;

                if (!string.IsNullOrWhiteSpace(data.CarTrasiteType))
                    PickerTrans.Text = data.CarTrasiteType;

                if (!string.IsNullOrWhiteSpace(data.IhavePelakNumber))
                    EntryIhavePelakNumber.Text = data.IhavePelakNumber;

            }
        }

        private async void PickerCarName_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("نام خودرو", "بستن", null, _baseData.CarName.Select(x => x.Title).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerCarName.Text = text;
                PickerTip.Text = "انتخاب کنید";
            }
        }

        private async void PickerTip_OnClicked(object sender, EventArgs e)
        {
            if (PickerCarName.Text.Equals("انتخاب کنید"))
            {
                await DisplayAlert("خطا", "نام خودرو را انتخاب کنید", "بستن");
                return;
            }

            var carName = _baseData.CarName.FirstOrDefault(x => x.Title == PickerCarName.Text);
            if (carName == null)
            {
                await DisplayAlert("خطا", "نام خودرو را انتخاب کنید", "بستن");
                return;
            }

            var text = await DisplayActionSheet("مدل", "بستن", null,
                carName.Brigades.Select(x => x.Title).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerTip.Text = text;
            }

        }

        private async void PickerSystem_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("سیستم", "بستن", null, _baseData.System.Select(x => x.Title).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerSystem.Text = text;
            }
        }

        private async void PickerYearCreate_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("سال ساخت", "بستن", null, _baseData.YearOfConstruction.Select(x => x.ToString()).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerYearCreate.Text = text;
            }
        }

        private async void PickerYearModle_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("مدل ساخت", "بستن", null, _baseData.YearOfConstruction.Select(x => x.ToString()).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerYearModle.Text = text;
            }
        }

        private async void PickerManufacturingCountry_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("کشور سازنده", "بستن", null, _baseData.ManufacturingCountry.Select(x => x.Title).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerManufacturingCountry.Text = text;
            }
        }

        private async void PickerColor_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("رنگ", "بستن", null, _baseData.Color.Select(x => x.Title).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerColor.Text = text;
            }
        }

        private async void PickerDoorCounts_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("تعداد درب خودرو", "بستن", null, _baseData.CarDoor.Select(x => x.ToString()).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerDoorCounts.Text = text;
            }
        }

        private async void PickerSeatsCounts_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("تعداد سرنشین", "بستن", null, _baseData.CarSeat.Select(x => x.ToString()).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerSeatsCounts.Text = text;
            }
        }

        private async void PickerAreaBuild_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("حوزه ساخت", "بستن", null, _baseData.AreaBuild.Select(x => x.Title).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerAreaBuild.Text = text;
            }
        }

        private async void PickerTrans_OnClicked(object sender, EventArgs e)
        {
            var text = await DisplayActionSheet("نوع واردات", "بستن", null, _baseData.Trans.Select(x => x.Title).ToArray());
            if (!string.IsNullOrWhiteSpace(text) && text != "بستن")
            {
                PickerTrans.Text = text;
            }
        }

        private void GoToLoginView(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new LoginView(), Navigation.NavigationStack[0]);
            Navigation.PopToRootAsync();
        }
    }
}
