﻿using System;
using System.Linq;
using System.Text;
using ArvandCarApp.Helpers;
using ArvandCarApp.Services.License;
using Syncfusion.ListView.XForms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArvandCarApp.Views.Profile.License
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LicenseMainView
    {
        private readonly LicenseMainViewModel _viewModel;

        public LicenseMainView()
        {
            InitializeComponent();

            BindingContext = _viewModel = new LicenseMainViewModel();

            #region BackPageButton

            var back = new TapGestureRecognizer();
            back.Tapped += BackPageButton_Clicked;
            BackPageButton.GestureRecognizers.Add(back);

            #endregion

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_viewModel.LicenseModels.Count == 0)
                _viewModel.LoadItemsCommand.Execute(null);
        }

        #region DataInfo_OnSelectionChanged

        private async void DataInfo_OnSelectionChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            var selected = DataInfo.SelectedItem;
            if (selected != null)
            {
                DataInfo.SelectedItem = null;

                var model = (LicenseModel)selected;
                var license = (await App.LicenseService.LoadAsync()).FirstOrDefault(x => x.Id == model.Id);
                if (license == null)
                    return;

                if (license.IsPaied)
                    return;

                var login = await App.LoginService.GetAsync();
                var plainTextBytes =
                    Encoding.UTF8.GetBytes(login.Username + "-" + login.Password + "-" + model.Id);
                Device.OpenUri(new Uri(ApiUrlHelper.Url + "/WebService/MobileCarLicense/Payment?q=" +
                                       Convert.ToBase64String(plainTextBytes)));
            }
        }

        #endregion

        #region BackPageButton Method

        private async void BackPageButton_Clicked(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        #endregion

    }
}