﻿using SQLite;

namespace ArvandCarApp.Services.Vacation.VacationPaymonyItem
{
    public class VacationPaymonyItemModel
    {
        [PrimaryKey]
        public long Id { get; set; }
        public string FkCar { get; set; }
        public string IsOk { get; set; }
        public string SetPayTypeActionSetPayTitle { get; set; }
        public string SaleReferenceId { get; set; }
        public string BankName { get; set; }
        public string SetPayRaveshVarizTitle { get; set; }
        public string SetPaymony { get; set; }
        public long UserPay { get; set; }
        public string SetPayDateStr { get; set; }
        public string SetpayTime { get; set; }
        public string SetPayTypeActionSetPay { get; set; }
        public string FkParant { get; set; }
    }
}
